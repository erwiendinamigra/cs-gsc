@echo off
rem ********************** email.bat *******************************
echo Dieser Batch (email.bat) dient als Beispiel fuer ein
echo Programm, das in der Lage ist, E-mail Adressen direkt
echo anzusprechen. Um ein echtes Programm zu konfigurieren,
echo sind die Einstellungen unter 
echo     System / Verwaltung / Configuration / Umgebung
echo entsprechend anzupassen. 
echo.
echo.
echo E-mail senden an: %1 %2 %3 %4 %5 %6 %7 %8 %9
echo.
echo.
pause
