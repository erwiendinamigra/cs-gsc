Attribute VB_Name = "WW_INTERFACE"

'***********************************************************************
'***********************************************************************
'***                                                                 ***
'***                      W W _ I N T E R F A C E                    ***
'***                                                                 ***
'***                C S   W A R E N W I R T S C H A F T              ***
'***             Schnittstelle Outlook - CS Warenwirtschaft          ***
'***                                                                 ***
'***   Stand: 23.12.2009 11:30                      Autor: Roman12   ***
'***                                                                 ***
'***********************************************************************
'***********************************************************************
'
'***********************************************************************
'**                                                                   **
'**   Copyright (C) CS, 2009-2010, All Rights Reserved, Confidential  **
'**                                                                   **
'***********************************************************************

'***********************************************************************
'***                       Parametrierung                            ***
'***********************************************************************
'--- Mail sichern ---
Const WW_BASEDIR          As String = "\\CS-SERVER\ww\"     ' Grundverzeichnis von CS-Warenwirtschaft
Const WW_CLIENT           As String = "001"                 ' Mandant von CS-Warenwirtschaft (=Mandanten Unterverzeichnis)
Const WW_CLIENTDIR        As String = "001"                 ' Subdirectory client (Normal=Client no, in case of inter-client addresses: "dat")
Const WW_USER             As String = "~outlook"            ' User von CS-Warenwirtschaft
Const WW_PASSWORD         As String = "~outlook"            ' Passwort dieses Users
Const WW_MAILDIR          As String = "Mails"               ' Unterverzeichnis fuer Mails in den Adressen
Const WW_ATTDIR           As String = "Files"               ' Unterverzeichnis fuer Attachments in den Adressen
Const WW_PROGRAMNAME      As String = "CS Warenwirtschaft"  ' Name von CS Warenwirtschaft
Const WW_LANGUAGE         As String = "D"                   ' Language "D" or "E"
Const WW_DEL_AFTER_SAVE   As Integer = 1                    ' Loeschen von Mails nach dem Sichern (0=kennzeichnen, 1=loeschen)
Const WW_DATE2FILENAME    As Integer = 0                    ' Datum in Filenamen? (0=nein, 1=am Beginn, 2=nach dem Sendername)
Const WW_SAVE2ALL         As Integer = 0                    ' Sichern unter allen Eintraegen? (0=Abbruch nach 1 gefundenen MailAdresse)
Const WW_SOUND            As Integer = 1                    ' Akustische Meldung nach dem Sichern (0=nein, 1=ja)
Const WW_SHOWPROGRESSBAR  As Integer = 1                    ' Progress-Bar anzeigen? (0=nein, 1=ja)

'------- Menue ------
Const WW_CMDNAME_SAVE     As String = "Save to &CS"         ' Name des Buttons zum Sichern von Mails
Const WW_CMDNAME_MENU     As String = "CS menu"             ' Name des Menus

'-------- Aux --------
Const WW_FILLCHR          As String = "_"                   ' Fuellzeichen in Dateinamen        (Should be "_")
Const WW_FILLCHR_DATE     As String = "_"                   ' Fuellzeichen in Datumsdarstellung (Should be "_")
'***********************************************************************
'***                     Ende Parametrierung                         ***
'***********************************************************************


Const WW_VERSION          As String = "V1.06"               ' Version der Schnittstelle
Const WW_KEY_MAIL         As Integer = 1                    ' Kennung fuer "Save Mail"
Const WW_KEY_ATT          As Integer = 2                    ' Kennung fuer "Save Attachments"
Const WW_KEY_ADRA_NEW     As Integer = 4                    ' Kennung fuer "New Activity"
Const WW_KEY_ADR_Z        As Integer = 8                    ' Kennung fuer "Zoom into Address"



Private Declare Function MakeSureDirectoryPathExists Lib "imagehlp.dll" (ByVal DirPath As String) As Long
Private Declare Function ShellExecute Lib "shell32.dll" _
     Alias "ShellExecuteA" (ByVal hwnd As Long, ByVal lpOperation As String, _
     ByVal lpFile As String, ByVal lpParameters As String, _
     ByVal lpDirectory As String, ByVal nShowCmd As Long) As Long



Option Explicit


' Stringeingabe: InputBox(...) as String



'-------------------------------------------------------------
' OPTIONS
'-------------------------------------------------------------
'Email format:
' MSG = Outlook msg format (incl. attachments, embedded objects etc.)., TXT = plain text
Private Const EXM_OPT_MAILFORMAT As String = "MSG"
'Date format of filename
Private Const EXM_OPT_FILENAME_DATEFORMAT As String = "yyyy-mm-dd_hh-nn-ss"
'Build filename; placeholders: <DATE> for date, <SENDER> for sender's name, <RECEIVER> for receiver, <SUBJECT> for subject
Private Const EXM_OPT_FILENAME_BUILD As String = "<DATE>_<SUBJECT>"
'Use browse folder? Set to FALSE if you don't want to use browser for selecting target folder
Private Const EXM_OPT_USEBROWSER As Boolean = True
'Target folder (used if EXM_OPT_USEBROWSER is set to FALSE)
Private Const EXM_OPT_TARGETFOLDER As String = "D:\"
'Maximum number of emails to be selected & exported. Please don't use a huge number as this will cause
'performance and maybe other issues. Recommended is a value between 5 and 20.
Private Const EXM_OPT_MAX_NO As Integer = 30
'Email subject prefixes (such us "RE:", "FW:" etc.) to be removed. Please note that this is a
'RegEx expression, google for "regex" for further information. For instance "\s" means blank " ".
Private Const EXM_OPT_CLEANSUBJECT_REGEX As String = "RE:\s|Re:\s|AW:\s|FW:\s|WG:\s|SV:\s|Antwort:\s"
'-------------------------------------------------------------


'-------------------------------------------------------------
' TRANSLATIONS
'-------------------------------------------------------------
'-- English
'Const EXM_007 = "Script terminated"
'Const EXM_013 = "Selected Outlook item is not an e-mail"
'Const EXM_014 = "File already exists"
'-- German
Private Const EXM_001 As String = "Die E-Mail wurde erfolgreich abgelegt."
Private Const EXM_002 As String = "Die E-Mail konnte nicht abgelegt werden, Grund:"
Private Const EXM_003 As String = "Ausgewählter Pfad:"
Private Const EXM_004 As String = "E-Mail(s) ausgewählt und erfolgreich abgelegt."
Private Const EXM_005 As String = "<FREE>"
Private Const EXM_006 As String = "<FREE>"
Private Const EXM_007 As String = "Script abgebrochen"
Private Const EXM_008 As String = "Fehler aufgetreten: Sie haben mehr als [LIMIT_SELECTED_ITEMS] E-Mails ausgewählt. Die Aktion wurde beendet."
Private Const EXM_009 As String = "Es wurde keine E-Mail ausgewählt."
Private Const EXM_010 As String = "Es ist ein Fehler aufgetreten: es war keine Email im Fokus, so dass die Ablage nicht erfolgen konnte."
Private Const EXM_011 As String = "Es ist ein Fehler aufgetreten:"
Private Const EXM_012 As String = "Die Aktion wurde beendet."
Private Const EXM_013 As String = "Ausgewähltes Outlook-Dokument ist keine E-Mail"
Private Const EXM_014 As String = "Datei existiert bereits"
Private Const EXM_015 As String = "<FREE>"
Private Const EXM_016 As String = "Bitte wählen Sie den Ordner zum Exportieren:"
Private Const EXM_017 As String = "Fehler beim Exportieren aufgetreten"
Private Const EXM_018 As String = "Export erfolgreich"
Private Const EXM_019 As String = "Bei [NO_OF_FAILURES] E-Mail(s) ist ein Fehler aufgetreten:"
Private Const EXM_020 As String = "[NO_OF_SELECTED_ITEMS] E-Mail(s) wurden ausgewählt und [NO_OF_SUCCESS_ITEMS] E-Mail(s) erfolgreich abgelegt."
'-------------------------------------------------------------


'-------------------------------------
'For browse folder
'-------------------------------------
'Private Declare Function lstrcat Lib "kernel32" Alias "lstrcatA" (ByVal lpString1 As String, ByVal lpString2 As String) As Long
'Private Declare Function SHBrowseForFolder Lib "shell32" (lpbi As BrowseInfo) As Long
'Private Declare Function SHGetPathFromIDList Lib "shell32" (ByVal pidList As Long, ByVal lpBuffer As String) As Long
'Private Declare Sub CoTaskMemFree Lib "ole32.dll" (ByVal hMem As Long)
'Private Const BIF_RETURNONLYFSDIRS = 1
'Private Const MAX_PATH = 260
'Private Type BrowseInfo
'
'    hwndOwner As Long
'    pIDLRoot As Long
'    pszDisplayName As Long
'    lpszTitle As Long
'    ulFlags As Long
'    lpfnCallback As Long
'    lParam As Long
'    iImage As Long
'End Type














'***********************************************************************
'Funktion: WW_MENUE_CREATE
'Eingaben: ---
'Ausgaben: ---
'Funktion: Menue anlegen
'***********************************************************************
Sub WW_MENUE_CREATE()

  Dim objCommandBar       As CommandBar
  Dim objCommandBarButton As CommandBarButton

  '-------------- "Save" Button ------------------
  Set objCommandBar = Application.ActiveExplorer.CommandBars("Standard")   ' Bearbeitung der Standard-Leiste
  Call WW_MENUE_DELETE                    ' Vorhandene Menu-Elemente loeschen

  Set objCommandBarButton = objCommandBar.Controls.Add( _
            Type:=msoControlButton, _
            temporary:=False)
  
  objCommandBarButton.Caption = WW_CMDNAME_SAVE                                  ' Name des Menus
  objCommandBarButton.TooltipText = "Save selected Mails to " & WW_PROGRAMNAME   ' Tooltip Text
  objCommandBarButton.Style = msoButtonIconAndCaption                            ' text + Icon
  objCommandBarButton.OnAction = "WW_SAVEMAIL"                                   ' Auszufuehrender Macroname
  objCommandBarButton.FaceId = 3                                                 ' welches Icon
'                                               3: Diskette
'                                              25: Lupe
'                                              48: Brille
'                                             352: Rote Lampe (Farbklecks)
                                                    
  '---------------- Dropdown Menu ----------------------
  Dim objCommandBarPopup As CommandBarPopup
  Dim objPopUpA As CommandBarPopup
  
  Set objCommandBarPopup = objCommandBar.Controls.Add( _
            Type:=msoControlPopup, _
            temporary:=False)
  objCommandBarPopup.Caption = WW_CMDNAME_MENU


  '---- Save Mail & Create Activity ----
  Dim objMenuButtonSaveMailAdra As CommandBarControl
  Set objMenuButtonSaveMailAdra = objCommandBarPopup.Controls.Add
  objMenuButtonSaveMailAdra.Caption = "Save mail && create &new activity"
  objMenuButtonSaveMailAdra.OnAction = "WW_SAVEMAIL_ADRA"
  objMenuButtonSaveMailAdra.Style = msoButtonIconAndCaption    ' msoButtonCaption
  objMenuButtonSaveMailAdra.FaceId = 8                                           ' 8 = Symbol "Leeres Fenster" / 18 = Symbol "New"

  '------- Zoom into Address ----------
  Dim objMenuButtonAdrZoom As CommandBarControl
  Set objMenuButtonAdrZoom = objCommandBarPopup.Controls.Add
  objMenuButtonAdrZoom.Caption = "&Zoom into address"
  objMenuButtonAdrZoom.OnAction = "WW_ADR_Z"
  objMenuButtonAdrZoom.Style = msoButtonIconAndCaption    ' msoButtonCaption
  objMenuButtonAdrZoom.FaceId = 1675                                             ' 1675 = Symbol "Brief" / 25 = Symbol "Lupe" / 105 = Symbol "Z"
  
  '---- Save Attachments ----
  Dim objMenuButtonSaveAtt As CommandBarControl
  Set objMenuButtonSaveAtt = objCommandBarPopup.Controls.Add
  objMenuButtonSaveAtt.Caption = "Save mail &attachments"
  objMenuButtonSaveAtt.OnAction = "WW_SAVEATT"
  objMenuButtonSaveAtt.Style = msoButtonIconAndCaption    ' msoButtonCaption
  objMenuButtonSaveAtt.FaceId = 53                                               ' 53 = Symbol 'mehrere Sheets'

  '---- Import ----
  Dim objMenuButtonImport As CommandBarControl
  Set objMenuButtonImport = objCommandBarPopup.Controls.Add
  objMenuButtonImport.Caption = "&Import from " & WW_PROGRAMNAME
  objMenuButtonImport.OnAction = "WW_IMPORT"
  objMenuButtonImport.Style = msoButtonIconAndCaption    ' msoButtonCaption
  objMenuButtonImport.FaceId = 39                                                ' 39 oder 133 = Symbol 'Pfeil nach rechts'

  '---- Display Config ----
  Dim objMenuButtonDispConfig As CommandBarControl
  Set objMenuButtonDispConfig = objCommandBarPopup.Controls.Add
  objMenuButtonDispConfig.BeginGroup = True
  objMenuButtonDispConfig.Caption = "Display configuration"
  objMenuButtonDispConfig.OnAction = "WW_DISPCONFIG"
  objMenuButtonDispConfig.Style = msoButtonIconAndCaption    ' msoButtonCaption
  objMenuButtonDispConfig.FaceId = 48                                                ' 48 = Brille
  
  '---- About ----
  Dim objMenuButtonAbout As CommandBarControl
  Set objMenuButtonAbout = objCommandBarPopup.Controls.Add
' objMenuButtonAbout.BeginGroup = True
  objMenuButtonAbout.Caption = "About " & WW_PROGRAMNAME & " interface"
  objMenuButtonAbout.OnAction = "WW_ABOUT"
  objMenuButtonAbout.Style = msoButtonIconAndCaption    ' msoButtonCaption
  objMenuButtonAbout.FaceId = 1954                                                ' 1954 = 'i'
  
  '------- alle FaceIDs anzeigen -----
'  Dim IDstart As Integer
'  Dim IDstop As Integer
'  Dim i As Integer
'  Dim NewButton As CommandBarControl
'
'  IDstart = 1            ' 1000 auf einmal geht
'  IDstop = 1000
'  For i = IDstart To IDstop
'    Set NewButton = objCommandBarPopup.Controls.Add
'    NewButton.Style = msoButtonIconAndCaption    ' msoButtonCaption
'    NewButton.FaceId = i
'    NewButton.Caption = "FaceID = " & i
'  Next i
 '-----------------------------------------

End Sub





'***********************************************************************
'Funktion: WW_MENUE_DELETE
'Eingaben: ---
'Ausgaben: ---
'Funktion: Menue loeschen
'***********************************************************************
Sub WW_MENUE_DELETE()
  On Error Resume Next
  Application.ActiveExplorer.CommandBars("Standard").Controls(WW_CMDNAME_SAVE).Delete
  Application.ActiveExplorer.CommandBars("Standard").Controls(WW_CMDNAME_MENU).Delete

  '------ alte Versionen loeschen ------
' Dim i As Integer                                  ' Alle MenuVarianten loeschen
' For i = 0 To 99
'   Dim bname As String
'   bname = WW_CMDNAME_MENU
'   If InStr(bname, "/") > 0 Then                   ' "/" enthalten?
'     bname = Left(bname, InStr(bname, "/") - 1)    ' nettoname bilden
'   End If                                          ' Ende "/" enthalten
'   If i > 0 Then
'     bname = bname & "/" & Format(i, "00")         ' Zweistellig darstellen
'   End If
'   Application.ActiveExplorer.CommandBars("Standard").Controls(bname).Delete   ' Alle Versionen loeschen
' Next
End Sub




'***********************************************************************
'Funktion: WW_ABOUT
'Eingaben: ---
'Ausgaben: ---
'Funktion: Info ueber die Schnittstelle
'***********************************************************************
Public Sub WW_ABOUT()
  msgbox WW_PROGRAMNAME & " Outlook interface" & Chr(13) & Chr(13) & _
         "Version " & WW_VERSION & Chr(13) & Chr(13) & _
         "Copyright Computer Service", _
         vbInformation, WW_PROGRAMNAME
End Sub






'***********************************************************************
'Funktion: WW_DISPCONFIG
'Eingaben: ---
'Ausgaben: ---
'Funktion: Parametrierung anzeigen
'***********************************************************************
Public Sub WW_DISPCONFIG()
  msgbox WW_PROGRAMNAME & " Outlook interface configuration" & Chr(13) & Chr(13) & _
         "CS Base directory " & Chr(9) & Chr(9) & WW_BASEDIR & Chr(13) & _
         "Client " & Chr(9) & Chr(9) & Chr(9) & WW_CLIENT & Chr(13) & _
         "User " & Chr(9) & Chr(9) & Chr(9) & WW_USER & Chr(13) & _
         "Mail sub-directory " & Chr(9) & Chr(9) & WW_MAILDIR & Chr(13) & _
         "Attachment sub-directory " & Chr(9) & WW_ATTDIR & Chr(13) & _
         "Programname " & Chr(9) & Chr(9) & WW_PROGRAMNAME & Chr(13) & _
         "Language " & Chr(9) & Chr(9) & WW_LANGUAGE & Chr(13) & _
         "Delete saved mails " & Chr(9) & Chr(9) & WW_DEL_AFTER_SAVE & Chr(13) & _
         "Mail date into filename " & Chr(9) & WW_DATE2FILENAME & Chr(13) & _
         "Save to all contacts " & Chr(9) & WW_SAVE2ALL & Chr(13) & _
         "Acustic feedback " & Chr(9) & Chr(9) & WW_SOUND & Chr(13) & _
         "Show progress bar " & Chr(9) & Chr(9) & WW_SHOWPROGRESSBAR, _
         vbInformation, WW_PROGRAMNAME
End Sub








'***********************************************************************
'Funktion: WW_IMPORT
'Eingaben: ---
'Ausgaben: ---
'Funktion: Dateien von CS Warenwirtschaft importieren
'***********************************************************************
Public Sub WW_IMPORT()
' DoCmd.TransferText [Transfertyp][, Spezifikationsname], Tabellenname, Dateiname[, Besitzt Feldnamen][, HTML-Tabellenname]
  msgbox "Import not installed" & Chr(13) & Chr(13) & _
         "Use Outlook's menu command 'File / Import/Export' instead" & Chr(13) & _
         "In the wizard select " & Chr(13) & _
         "   - Import from other Programs" & Chr(13) & _
         "   - Tab delimited (Windows)" & Chr(13) & _
         "   - Replace duplicates by imported records" & Chr(13) & _
         "   - Folder: Contacts" & Chr(13) & _
         "   - Filename: <WW basedir>\outlook_adressen.txt" & Chr(13), _
         vbInformation, WW_PROGRAMNAME
End Sub



'***********************************************************************
'Funktion: WW_SAVEMAIL
'Eingaben: ---
'Ausgaben: ---
'Funktion: Nach Betaetigung des zugehoerigen Buttons Mail in
'          CS Warenwirtschaft sichern
'***********************************************************************
Public Sub WW_SAVEMAIL()
  WW_SAVE_X WW_KEY_MAIL                             ' Mails sichern
End Sub




'***********************************************************************
'Funktion: WW_SAVEMAIL_ADRA
'Eingaben: ---
'Ausgaben: ---
'Funktion: Nach Betaetigung des zugehoerigen Buttons Mail in
'          CS Warenwirtschaft sichern.
'          Danach neue Aktivitaet mit der Mail als Datei-Referenz oeffnen
'***********************************************************************
Public Sub WW_SAVEMAIL_ADRA()
  WW_SAVE_X WW_KEY_MAIL Or WW_KEY_ADRA_NEW    ' Mails sichern + Neue Aktivitaet
End Sub





'***********************************************************************
'Funktion: WW_SAVEATT
'Eingaben: ---
'Ausgaben: ---
'Funktion: Nach Betaetigung des zugehoerigen Buttons Attachments in
'          CS Warenwirtschaft sichern
'***********************************************************************
Public Sub WW_SAVEATT()
  WW_SAVE_X WW_KEY_ATT                              ' Attachments sichern
End Sub




'***********************************************************************
'Funktion: WW_ADR_Z
'Eingaben: ---
'Ausgaben: ---
'Funktion: Zoom in Adresse
'***********************************************************************
Public Sub WW_ADR_Z()
  WW_SAVE_X WW_KEY_ADR_Z                             ' Zoom in address
End Sub





'***********************************************************************
'Funktion: WW_SAVE_X
'Eingaben: 1. Kennung, was zu sichern ist
'Ausgaben: ---
'Funktion: Nach Betaetigung des zugehoerigen Buttons Mail in
'          CS Warenwirtschaft sichern
'***********************************************************************
Public Sub WW_SAVE_X(ikey As Integer)
    Const PROCNAME As String = "WW_SAVEMAIL"
    
'    On Error GoTo ErrorHandler
    
    Dim myExplorer As Outlook.Explorer
    Dim myfolder As Outlook.MAPIFolder
    Dim myItem As Object
    Dim olSelection As Selection
    Dim strBackupPath As String
    Dim intCountAll As Integer
    Dim intCountFailures As Integer
    Dim strStatusMsg As String
    Dim vSuccess As Variant
    Dim strTemp1 As String
    Dim strTemp2 As String
    Dim strErrorMsg As String
 
    If WW_SHOWPROGRESSBAR <> 0 Then                        ' Mit Fortschrittsbalken?
      Load WW_PROGRESSFORM
      WW_PROGRESSFORM.Caption = WW_PROGRAMNAME          ' meine Ueberschrift
      WW_PROGRESSFORM.Show vbModeless
    End If                                              ' ende mit Fortschrittsbalken
    
    '-------------------------------------
    'Get target drive
    '-------------------------------------
'   If (EXM_OPT_USEBROWSER = True) Then
'        strBackupPath = GetFileDir
'        If Left(strBackupPath, 15) = "ERROR_OCCURRED:" Then
'            strErrorMsg = Mid(strBackupPath, 16, 9999)
'            Error 5004
'        End If
'    Else
'        strBackupPath = EXM_OPT_TARGETFOLDER
'    End If
'    If strBackupPath = "" Then GoTo ExitScript
'    If (Not Right(strBackupPath, 1) = "\") Then strBackupPath = strBackupPath & "\"
    
    
 
    '-------------------------------------
    'Process according to what is in the focus: an opened e-mail or a folder with selected e-mails.
    'Case 2 would also work for opened e-mail, however it does not always work (for instance if
    ' an e-mail is saved on the file system and being opened from there).
    '-------------------------------------

    Set myExplorer = Application.ActiveExplorer
    Set myfolder = myExplorer.CurrentFolder
    If myfolder Is Nothing Then Error 5001
    If Not myfolder.DefaultItemType = olMailItem Then GoTo ExitScript
    
    '----- In-/Out Mail ------
    Dim ioutmail As Integer
    ioutmail = 1                                ' default: Ausgangsmail
    If ((StrComp(myfolder.Name, "Posteingang", vbTextCompare) = 0) Or (StrComp(myfolder.Name, "Inbox", vbTextCompare) = 0)) Then
      ioutmail = 0                              ' Eingangsmail
    End If                                      ' Eingangsmail
    '-------------------------
    
    'Stop if more than x emails selected
    If myExplorer.Selection.Count > EXM_OPT_MAX_NO Then Error 5002
      
    'No email selected at all?
    If myExplorer.Selection.Count = 0 Then Error 5003
     
    Set olSelection = myExplorer.Selection
    intCountAll = 0
    intCountFailures = 0
    For Each myItem In olSelection
      If TypeName(myItem) = "MailItem" Then         ' Nur (!) Mails
'        TypeName(myItem) = "ReportItem" Then       ' Das sind z.B. die "Gelesen: ..." Mails
' oder
'     If TypeOf myItem Is Outlook.MailItem Then
        intCountAll = intCountAll + 1
        
        '---- alles ist ausgehende Mail ----        ' das umgeht das Problem, dass bei Outlook 2000 der Absender nicht verfuegbar ist
'       If ioutmail = 0 Then                        ' ankommende Mail?
'         Dim objMailItem1 As MailItem
'         Set objMailItem1 = myItem.Reply           ' mach eine Antwortmail draus
'         Set myItem = objMailItem1
'         ioutmail = 1                              ' jetzt ist es eine ausgehende Mail
'       End If                                      ' ende ankommende Mail
        '--- ~alles ist ausgehende Mail ----
        
        Dim i As Integer
        If ioutmail <> 0 Then                       ' Ausgangsmail
          i = WW_SAVE1OUTMAIL(ikey, myItem)         ' diese Mail sichern
        Else                                        ' eingangsmail
          i = WW_SAVE1INMAIL(ikey, myItem)          ' Diese Mail sichern
        End If                                      ' ende eingangsmail
        If (i <> 0) Then
          intCountFailures = intCountFailures + 1
        End If
      End If                                        ' Ende Objekt ist eine Email
    Next


ExitScript:
    If WW_SHOWPROGRESSBAR <> 0 Then                        ' Mit Fortschrittsbalken?
      WW_PROGRESSFORM.Hide                              ' Fortschrittsbalken nicht mehr anzeigen
    End If                                              ' ende mit Fortschrittsbalken
    Exit Sub

ErrorHandler:
    Select Case Err.Number
    Case 5001:  'Not an email
        msgbox EXM_010, 64, EXM_007
    Case 5002:
        msgbox Replace(EXM_008, "[LIMIT_SELECTED_ITEMS]", EXM_OPT_MAX_NO), 64, EXM_007
    Case 5003:
        msgbox EXM_009, 64, EXM_007
    Case 5004:
        msgbox EXM_011 & Chr(10) & Chr(10) & strErrorMsg, 48, EXM_007
    Case Else:
        msgbox EXM_011 & Chr(10) & Chr(10) _
        & Err & " - " & Error$ & Chr(10) & Chr(10) & EXM_012, 48, EXM_007
    End Select
    Resume ExitScript
End Sub



'***********************************************************************
'Funktion: WW_SHOWPROGRESS
'Eingaben: 1. Control-Object in einem Formular
'          2. aktueller Wert
'          3. Maximalwertt
'Ausgaben: ---
'Funktion: Anzeigen eines simulierten Fortschrittsbalkens im Control-Objekt
'***********************************************************************
Sub WW_SHOWPROGRESS(objControl As Control, iakt As Integer, imax As Integer)
  Dim imax1 As Integer                              ' verhindere Div durch Null
  imax1 = imax
  If (imax1 < 1) Then imax1 = 1
  
  Dim iProz As Integer
  iProz = Int((iakt / imax1) * 100 + 0.5)
  If (iProz < 0) Then iProz = 0
  If (iProz > 100) Then iProz = 100
 
  objControl.Caption = String$(Int((Val(objControl.Tag) / 100) * iProz + 0.5), "n")
  DoEvents                                          ' alles aktuell anzeigen
End Sub







'***********************************************************************
'Funktion: WW_STRPATCH
'Eingaben: 1. GesamtString
'          2. Zu ersetzender Teil des Strings
'          3. Ersatzstring
'Ausgaben: Veraenderter String
'Funktion: String veraendern
'***********************************************************************
Function WW_STRPATCH(ByVal b As String, ByVal b1 As String, ByVal b2 As String) As String
  '----- entweder -----
  WW_STRPATCH = Replace(b, b1, b2, 1, -1, vbTextCompare)
  '------- oder -------
'  Dim bb As String
'  bb = ""                               ' Neuer String
'
'  Dim i As Integer
'  i = 1
'  Do
'    If (StrComp(b1, Mid(b, i, Len(b1)), vbTextCompare) = 0) Then
'      bb = bb & b2
'      i = i + Len(b1)
'    Else
'      bb = bb & Mid(b, i, 1)
'      i = i + 1
'    End If
'  Loop Until i > Len(b)                ' bis zum Stringende
'  WW_STRPATCH = bb
  '---------------------
End Function




'***********************************************************************
'Funktion: WW_TESTDIR
'Eingaben: 1. Pfad
'Ausgaben: ---
'Funktion: Verzeichnis anlegen, wenn nicht vorhanden
'***********************************************************************
Sub WW_TESTDIR(ByVal b As String)
  Dim lReturn As Long
  lReturn = MakeSureDirectoryPathExists(b)
  If lReturn <> 1 Then msgbox "Error creating directory >" & b, vbCritical, WW_PROGRAMNAME
End Sub

'*************** ODER ************
'Die folgende Funktion gibt einen Wahrheitswert zurück, ob ein Unterverzeichnis existiert oder nicht. Sollte es nicht existieren wird es erstellt. Dies funktioniert auch auf mehreren Ebenen.Code:
'Public Function CheckDIR(ByVal Verzeichnis As String) As Boolean
'    On Error GoTo CheckDIR_Exit
'
'    Dim i As Integer
'    Dim strNewVerzeichnis  As String
'
'    If Right(Verzeichnis, 1) <> "\" Then Verzeichnis = Verzeichnis & "\"
'
'    i = InStr(4, Verzeichnis, "\")
'    While i > 0
'        strNewVerzeichnis = Left(Verzeichnis, i)
'        If Len(Dir(strNewVerzeichnis, vbDirectory)) = 0 Then MkDir (strNewVerzeichnis)
'        i = InStr(i + 1, Verzeichnis, "\")
'    Wend
'
'CheckDIR_Exit:
'    CheckDIR = (Err.Number = 0)
'End Function
'
'------------------------
'Die folgende Prozedur zeigt, wie die obere Funktion benutzt wird:Code:
'Sub Test()
'    Dim strVerzeichnis As String
'
'    strVerzeichnis = InputBox("Verzeichnis:")
'    If CheckDIR(strVerzeichnis) Then
'        ChDir strVerzeichnis
'    Else
'        MsgBox "Verzeichnis konnte nicht erstellt werden!"
'    End If
'End Sub









'***********************************************************************
'Funktion: WW_MAKEFILENAME
'Eingaben: 1. Kennung, was zu sichern ist
'          2. Mail, die zu sichern ist
'          3. zugehoeriger Kontakt
'          4. Falls Attachments gewaehlt sind: Nr des Attachments
'Ausgaben: 0:OK, <>0:Fehler
'Funktion: Mail in CS Warenwirtschaft sichern
'***********************************************************************
Private Function WW_MAKEFILENAME(ikey As Integer, objMailItem As Outlook.MailItem, objContact As Outlook.ContactItem, iatt As Integer) As String
  Dim objOutlook      As Outlook.Application
  Dim objNameSpace    As Outlook.NameSpace
  
  Set objOutlook = New Outlook.Application
  Set objNameSpace = objOutlook.GetNamespace("MAPI")
  
  Dim bSubject As String
  bSubject = objMailItem.Subject
          
  bSubject = WW_STRPATCH(bSubject, ":", " ")
  bSubject = WW_STRPATCH(bSubject, "/", " ")
  bSubject = WW_STRPATCH(bSubject, "\", " ")
  bSubject = WW_STRPATCH(bSubject, ">", " ")
  bSubject = WW_STRPATCH(bSubject, "<", " ")
  bSubject = WW_STRPATCH(bSubject, "|", " ")
  bSubject = WW_STRPATCH(bSubject, "'", "")
  bSubject = WW_STRPATCH(bSubject, "*", " ")
  bSubject = WW_STRPATCH(bSubject, "?", " ")
  bSubject = WW_STRPATCH(bSubject, """", " ")

  Dim bsaveas As String
  bsaveas = WW_BASEDIR                    ' Basis-Verzeichnis
  If (Right(bsaveas, 1) <> "\") Then      ' Kontrolle auf '\'
    bsaveas = bsaveas & "\"
  End If
  bsaveas = bsaveas & WW_CLIENTDIR        ' Mandanten-Verzeichnis
  If (Right(bsaveas, 1) <> "\") Then      ' Kontrolle auf '\'
    bsaveas = bsaveas & "\"
  End If
  bsaveas = bsaveas & "adr"               ' Alles in den Adressen der Warenwirtschaft
  bsaveas = bsaveas & "\"
  If (objContact.OrganizationalIDNumber <> "") Then         ' nur wenn AdressNr vorhanden
    bsaveas = bsaveas & objContact.OrganizationalIDNumber   ' Zu dieser Adresse
    bsaveas = bsaveas & "\"
  End If
  
  If ((ikey And WW_KEY_MAIL) <> 0) Then
    bsaveas = bsaveas & WW_MAILDIR        ' Unterverzeichnis fuer Mails
  End If
  If ((ikey And WW_KEY_ATT) <> 0) Then
    bsaveas = bsaveas & WW_ATTDIR         ' Unterverzeichnis fuer Attachments
  End If
  
  If (Right(bsaveas, 1) <> "\") Then      ' Kontrolle auf '\'
    bsaveas = bsaveas & "\"
  End If
  WW_TESTDIR bsaveas                      ' Verzeichnis anlegen, falls nicht vorhanden
  
  If ((ikey And WW_KEY_MAIL) <> 0) Then
    If WW_DATE2FILENAME = 1 Then            ' Maildatum am Beginn des Filenamens
      bsaveas = bsaveas & Year(objMailItem.ReceivedTime) & WW_FILLCHR_DATE & Format(Month(objMailItem.ReceivedTime), "00") & WW_FILLCHR_DATE & Format(Day(objMailItem.ReceivedTime), "00") & WW_FILLCHR
    End If                                  ' ende Maildatum am Beginn des Dateinamens

'   bsaveas = bsaveas & objNameSpace.CurrentUser            ' Sender der Mail
    
'   If Len(objMailItem.SenderName) <> 0 Then                ' Normalfall
    If objMailItem.Sent = True Then                         ' Normalfall
      bsaveas = bsaveas & objMailItem.SenderName            ' Sender der Mail
    Else                                                    ' Workaround: z.B. einen Entwurf unter Outlook 2000 anstatt incoming Mail
      bsaveas = bsaveas & objContact.FullName               ' Da kam die Mail ursprunglich her
    End If                                                  ' ende Workaround
    bsaveas = bsaveas & WW_FILLCHR
    
    If WW_DATE2FILENAME = 2 Then            ' Maildatum nach dem SenderName
      bsaveas = bsaveas & Year(objMailItem.ReceivedTime) & WW_FILLCHR_DATE & Format(Month(objMailItem.ReceivedTime), "00") & WW_FILLCHR_DATE & Format(Day(objMailItem.ReceivedTime), "00") & WW_FILLCHR
    End If                                  ' ende Maildatum nach dem SenderName
    
    bsaveas = bsaveas & bSubject            ' Subject anhaengen
    bsaveas = bsaveas & ".msg"              ' Extension"
  End If                                    ' ende Mail sichern
  If ((ikey And WW_KEY_ATT) <> 0) Then
    Dim objAttachment As Attachment
    Set objAttachment = objMailItem.Attachments(iatt)
    bsaveas = bsaveas & objAttachment.Filename
  End If                                    ' ende Attachment sichern
  
  bsaveas = WW_STRPATCH(bsaveas, " ", WW_FILLCHR)  ' kein space im Dateinamen

  '--------- Test, ob Filename bereits existiert -------
  If ((ikey And WW_KEY_MAIL) <> 0) Then      ' nur bei Emails
    Dim iok  As Integer
    Dim icnt As Integer
    iok = 0
    icnt = 0
    Do
      Dim b As String
      b = bsaveas
    
      Dim blfd As String
      blfd = ""
      If icnt > 0 Then
        blfd = WW_FILLCHR
        blfd = blfd + Format(icnt, "00")    ' Zweistellig darstellen
      End If
      blfd = blfd + ".msg"
    
      b = WW_STRPATCH(b, ".msg", blfd)
      If Len(Dir(b, vbNormal + vbReadOnly)) = 0 Then  ' Test, ob Datei vorhanden
        iok = 1                             ' nicht vorhandener Dateiname gefunden
        bsaveas = b                         ' diesen Dateinamen verwenden
      End If                                ' ende Datei nicht vorhanden
      icnt = icnt + 1                       ' naechste Datei versuchen
    Loop Until iok <> 0 And icnt < 999
  End If                                    ' ende Email ist zu sichern
  '-------- ~Test, ob Filename bereits existiert -------

  WW_MAKEFILENAME = bsaveas
End Function





'***********************************************************************
'Funktion: WW_SAVE1OUTMAIL
'Eingaben: 1. Kennung, was zu sichern ist
'          2. Mail, die zu sichern ist
'Ausgaben: 0:OK, <>0:Fehler
'Funktion: Mail in CS Warenwirtschaft sichern
'***********************************************************************
Private Function WW_SAVE1OUTMAIL(ikey As Integer, objMailItem As MailItem) As Integer
  WW_SAVE1OUTMAIL = 0                          ' default: OK
  
' If (ido <> 0) Then
    Dim idone As Integer
    idone = 0
    
    '----------- Empfaenger der Mail ----------------
    Dim objRecipients As Recipients
    Set objRecipients = objMailItem.Recipients
    Dim objRecipient As Recipient
    For Each objRecipient In objRecipients
      If objRecipient.Resolve Then
        If Left(objRecipient.Address, 1) <> "/" Then     ' interne Codierung beginnt mit "/O=CS-COMP...."
        
          '------------ Contacts -----
          Dim objOutlook      As Outlook.Application
          Dim objNameSpace    As Outlook.NameSpace
          Dim objContact      As ContactItem
          Dim objContactsFolder As Object
        
          Set objOutlook = New Outlook.Application
          Set objNameSpace = objOutlook.GetNamespace("MAPI")
          Set objContactsFolder = objNameSpace.GetDefaultFolder(olFolderContacts)
        
'         Set objContact = objContactsFolder.Items.Find("[FileAs] = objRecipient.Name")   ' nicht moeglich
          Dim iContactCnt As Integer
          iContactCnt = 0                           ' Zaehler fuer kontakte
          
          Dim icontactfound As Integer
          icontactfound = 0
          Dim objContact1 As ContactItem
          For Each objContact1 In objContactsFolder.Items
            iContactCnt = iContactCnt + 1
            If WW_SHOWPROGRESSBAR <> 0 Then            ' mit Progress-Bar?
              WW_SHOWPROGRESS WW_PROGRESSFORM.WW_PROGRESSBAR, iContactCnt, objContactsFolder.Items.Count    ' Fortschritt anzeigen
            End If                                  ' ende mit Progress-Bar
            
'           If (icontactfound = 0) Then
            
              '---- Email-Adr in der Mail ----
              Dim bEmailAdresse As String         ' Email-Adr in der Mail
              bEmailAdresse = objRecipient.Address
              WW_STRPATCH bEmailAdresse, "'", ""  ' Unbekannte Mailadresse stehen in einfachen Hochkommas
              '---- Emailadr im Kontakt ------
              Dim bEmailContact As String
              bEmailContact = objContact1.Email1Address
              If InStr(bEmailContact, " ") > 0 Then        ' Leerzeichen enthalten?
                bEmailContact = Left(bEmailContact, InStr(bEmailContact, " ") - 1)
              End If                               ' Ende Leerzeichen enthalten
            
              Dim bEmailContact2 As String
              bEmailContact2 = objContact1.Email2Address
              If InStr(bEmailContact2, " ") > 0 Then        ' Leerzeichen enthalten?
                bEmailContact2 = Left(bEmailContact2, InStr(bEmailContact2, " ") - 1)
              End If                               ' Ende Leerzeichen enthalten
            
              Dim bEmailContact3 As String
              bEmailContact3 = objContact1.Email3Address
              If InStr(bEmailContact3, " ") > 0 Then        ' Leerzeichen enthalten?
                bEmailContact3 = Left(bEmailContact3, InStr(bEmailContact3, " ") - 1)
              End If                               ' Ende Leerzeichen enthalten
              '-------------------------------

              '----- debug -----
'             If Len(objContact1.OrganizationalIDNumber) > 0 And Len(bEmailContact) > 0 Then
'               Debug.Print "Suche nach >" & bEmailAdresse & "<    Kontakt >" & objContact1.OrganizationalIDNumber & "< >" & bEmailContact & "<  " & bEmailContact2 & "   " & bEmailContact3
'             End If
              '-----------------
            
'             If UCase(Contact1.FullName) = UCase(objRecipient.Name) Then
              If ((UCase(bEmailContact) = UCase(bEmailAdresse)) Or _
                  (UCase(bEmailContact2) = UCase(bEmailAdresse)) Or _
                  (UCase(bEmailContact3) = UCase(bEmailAdresse))) Then
                icontactfound = 1
                Set objContact = objContact1
                
                '--- found ---
                If (objContact.OrganizationalIDNumber = "") Then       ' nur wenn AdressNr vorhanden
                  msgbox "Contact >" & objRecipient.Name & "< : No organizational ID number", vbOKOnly, WW_PROGRAMNAME
                  WW_SAVE1OUTMAIL = 2                 ' keine Adress-Nr vorhanden
                Else                                  ' Adress-Nr gefunden
                  Dim bsaveas As String
                  If ((ikey And WW_KEY_MAIL) <> 0) Then  ' Mail sichern
                    bsaveas = WW_MAKEFILENAME(ikey, objMailItem, objContact, 0)   ' Dateiname incl Pfad generieren
                    objMailItem.SaveAs bsaveas, olMSG ' als Datei speichern
                    idone = 1                         ' gesichert
                  End If                              ' ende Mail sichern
                  
                  If ((ikey And WW_KEY_ATT) <> 0) Then   ' Attachments sichern
                    Dim iatt As Integer
                    If objMailItem.Attachments.Count > 0 Then
                      For iatt = 1 To objMailItem.Attachments.Count
                        Dim objAttachment As Attachment
                        Set objAttachment = objMailItem.Attachments(iatt)
                      
                        bsaveas = WW_MAKEFILENAME(ikey, objMailItem, objContact, iatt)   ' Dateiname incl Pfad generieren
                        objAttachment.SaveAsFile bsaveas                                ' Attachment sichern
                      Next                            ' naechstes Attachment
                    End If                            ' ende Attachment vorhanden
                    idone = 1                         ' gesichert
                  End If                              ' ende Attachments sichern
                  
                  If ((ikey And WW_KEY_ADR_Z) <> 0) Then      ' Zoom into Adr
                    Dim ww_name As String
                    ww_name = WW_BASEDIR
                    If (Right(ww_name, 1) <> "\") Then        ' Kontrolle auf '\'
                      ww_name = ww_name & "\"
                    End If
                    ww_name = ww_name & "wwfi.exe"            ' Programmname
                    If (WW_LANGUAGE <> "D") Then
                      WW_STRPATCH ww_name, "wwfi.exe", "wwfi_e.exe"
                    End If
  
                    Dim ww_param As String
                    ww_param = ""
                    ww_param = ww_param & " -f" & WW_CLIENT   ' Parameter: Firma
                    ww_param = ww_param & " -b" & WW_USER     ' Parameter: User
                    If (Len(WW_PASSWORD) <> 0) Then
                      ww_param = ww_param & " -p" & WW_PASSWORD  ' Parameter: Passwort
                    End If
                    ww_param = ww_param & " ""-iadr_z;" & objContact.OrganizationalIDNumber & """"

                    ShellExecute 0, "open", ww_name, ww_param, WW_BASEDIR, 0&
                  End If                              ' ende Funktion
                  
                  If ((ikey And WW_KEY_ADRA_NEW) <> 0) Then   ' neue Aktivitaet
'                   Dim ww_name As String
                    ww_name = WW_BASEDIR
                    If (Right(ww_name, 1) <> "\") Then        ' Kontrolle auf '\'
                      ww_name = ww_name & "\"
                    End If
                    ww_name = ww_name & "wwfi.exe"            ' Programmname
                    If (WW_LANGUAGE <> "D") Then
                      WW_STRPATCH ww_name, "wwfi.exe", "wwfi_e.exe"
                    End If
  
'                   Dim ww_param As String
                    ww_param = ""
                    ww_param = ww_param & " -f" & WW_CLIENT   ' Parameter: Firma
                    ww_param = ww_param & " -b" & WW_USER     ' Parameter: User
                    If (Len(WW_PASSWORD) <> 0) Then
                      ww_param = ww_param & " -p" & WW_PASSWORD  ' Parameter: Passwort
                    End If
                    ww_param = ww_param & " ""-iadra_new;" & objContact.OrganizationalIDNumber & ";" & bsaveas & """"

                    ShellExecute 0, "open", ww_name, ww_param, WW_BASEDIR, 0&
                  End If                              ' ende Funktion
                  
                  If WW_SOUND <> 0 Then               ' Mit Sound?
                    Beep                              ' Ton
                  End If                              ' ende mit Sound
                End If                                ' Ende Adress-Nr vorhanden
                '-- ~found ---
               
                If WW_SAVE2ALL = 0 Then           ' nur an 1 Email sichern?
                  Exit For                        ' ende suche nach kontakt
                End If                            ' ende nur an 1 Email sichern
              End If                              ' ende Contakt passt
'           End If                                ' ende noch nichts gefunden
          Next                                    ' naechster Contact
        
          If (icontactfound = 0) Then
            msgbox "Contact >" & objRecipient.Name & "< not found", vbOKOnly, WW_PROGRAMNAME
            WW_SAVE1OUTMAIL = 1                   ' nicht in den Kontakten
          End If                                ' ende Kontakt gefunden
        End If                                  ' ende suche auch nach dieser Emailadresse
      End If                                    ' ende Email-Adresse aufgeloest
    Next                                        ' Naechster Empfaenger
              
    If idone <> 0 Then                          ' gesichert?
      If ((ikey And WW_KEY_MAIL) <> 0) Then     ' nur beim sichern von Mails
        If WW_DEL_AFTER_SAVE <> 0 Then          ' gleich loeschen?
          objMailItem.Delete                    ' Mail loeschen
        Else                                    ' nicht loeschen
          objMailItem.FlagStatus = olFlagComplete   ' als erledigt markieren
'         objMailItem.FlagIcon   = olYellowFlagIcon ' ab Outlook 2003 besteht auch diese Moeglichkeit
          objMailItem.Save                      ' Aenderung abspeichern
        End If                                  ' ende nicht Loeschen
      End If                                    ' ende Mail sichern
    End If                                      ' ende gesichert
' End If                                        ' Ende Sicherung ausfuehren
End Function








'***********************************************************************
'Funktion: WW_SAVE1INMAIL
'Eingaben: 1. Kennung, was zu sichern ist
'          2. Mail, die zu sichern ist
'Ausgaben: 0:OK, <>0:Fehler
'Funktion: Eingegangene Mail in CS Warenwirtschaft sichern
'***********************************************************************
Private Function WW_SAVE1INMAIL(ikey As Integer, objMailItem As Outlook.MailItem) As Integer
  WW_SAVE1INMAIL = 0                           ' default: OK
' On Error Resume Next

  Dim objOutlook        As Outlook.Application
  Dim objNameSpace      As Outlook.NameSpace
  Dim objContact        As ContactItem
  Dim objContactsFolder As Object
        
  Set objOutlook = New Outlook.Application
  Set objNameSpace = objOutlook.GetNamespace("MAPI")
' Set objInboxfolder = objNameSpace.GetDefaultFolder(olFolderInbox)

' If ido <> 0 Then
    Dim idone As Integer
    idone = 0

'   Dim bprompt As String
'   bprompt = "New mail:" & Chr(13) & Chr(13) & "Sender: " & objMailItem.SenderName & Chr(13) & "Subject: " & objMailItem.Subject & Chr(13) & Chr(13) & "Save to " & WW_PROGRAMNAME & " ?"
'   Dim iresult As Integer
'   iresult = MsgBox(bprompt, vbQuestion + vbYesNo + vbDefaultButton1, WW_PROGRAMNAME)
'   If (iresult = vbYes) Then
      Set objContactsFolder = objNameSpace.GetDefaultFolder(olFolderContacts)
        
'     Set objContact = objcontactsfolder.Items.Find("[FileAs] = myRecipient.Name")   ' nicht moeglich
      
'     Set myReplyRecipients = objMail.ReplyRecipients
'     For Each myReplyRecipient In myReplyRecipients
'       If myReplyRecipient.Resolve Then
          
          Dim iContactCnt As Integer
          iContactCnt = 0                           ' Zaehler fuer kontakte
          
          Dim icontactfound   As Integer
          icontactfound = 0
          
          '---- Email-Adr in der Mail ----
          Dim bEmailAdresse As String         ' Email-Adr in der Mail
'         bEmailAdresse = objMailItem.SenderName           ' !!!! Nur Name, nicht die Mailadresse
          bEmailAdresse = ""
          
          '---entweder---
'         bEmailAdresse = objMailItem.SenderEmailAddress   ' !!!! Erst ab Outlook 2003
          '-----oder-----
          Dim objMailItemReply As MailItem      ' hier ist die AntwortMail
          Set objMailItemReply = objMailItem.Reply              ' Antwort erzeugen
              
          Dim objRecipientsReply As Recipients                  ' mach eine AntwortMail draus und verwende den Empfaenger
          Set objRecipientsReply = objMailItemReply.Recipients
          
          Dim objRecipientReply As Recipient
          Set objRecipientReply = objRecipientsReply.Item(1)
          If objRecipientReply.Resolve Then
            If Left(objRecipientReply.Address, 1) <> "/" Then   ' interne Codierung beginnt mit "/O=CS-COMP...."
              bEmailAdresse = objRecipientReply.Address
            End If                                              ' ende keine Exchange adresse
          End If                                                ' ende Mailadresse aufgeloest
          '--------------
          
          WW_STRPATCH bEmailAdresse, "'", ""  ' Unbekannte Mailadressen stehen in einfachen Hochkommas
          If Len(bEmailAdresse) <> 0 Then       ' gibt es eine Mailadresse?
          
            Dim objContact1 As ContactItem
            For Each objContact1 In objContactsFolder.Items
              iContactCnt = iContactCnt + 1
              If WW_SHOWPROGRESSBAR <> 0 Then            ' mit Progress-Bar?
                WW_SHOWPROGRESS WW_PROGRESSFORM.WW_PROGRESSBAR, iContactCnt, objContactsFolder.Items.Count    ' Fortschritt anzeigen
              End If                                  ' ende mit Progress-Bar

              '---- Emailadr im Kontakt ------
              Dim bEmailContact As String
              bEmailContact = objContact1.Email1Address
              If InStr(bEmailContact, " ") > 0 Then        ' Leerzeichen enthalten?
                bEmailContact = Left(bEmailContact, InStr(bEmailContact, " ") - 1)
              End If                               ' Ende Leerzeichen enthalten
            
              Dim bEmailContact2 As String
              bEmailContact2 = objContact1.Email2Address
              If InStr(bEmailContact2, " ") > 0 Then        ' Leerzeichen enthalten?
                bEmailContact2 = Left(bEmailContact2, InStr(bEmailContact2, " ") - 1)
              End If                               ' Ende Leerzeichen enthalten
            
              Dim bEmailContact3 As String
              bEmailContact3 = objContact1.Email3Address
              If InStr(bEmailContact3, " ") > 0 Then        ' Leerzeichen enthalten?
                bEmailContact3 = Left(bEmailContact3, InStr(bEmailContact3, " ") - 1)
              End If                               ' Ende Leerzeichen enthalten
              '-------------------------------

              '----- debug -----
'             If Len(objContact1.OrganizationalIDNumber) > 0 And Len(bEmailContact) > 0 Then
'               Debug.Print "Suche nach >" & bEmailAdresse & "<    Kontakt >" & objContact1.OrganizationalIDNumber & "< >" & bEmailContact & "<"
'             End If
              '-----------------
            
'             If UCase(Contact1.FullName) = UCase(objRecipient.Name) Then
              If ((UCase(bEmailContact) = UCase(bEmailAdresse)) Or _
                  (UCase(bEmailContact2) = UCase(bEmailAdresse)) Or _
                  (UCase(bEmailContact3) = UCase(bEmailAdresse)) Or _
                  (UCase(objContact1.FullName) = UCase(bEmailAdresse))) Then     ' Kruecke, da Email nicht vorhanden
                icontactfound = 1
                Set objContact = objContact1
                
                '--- found ---
                If (objContact.OrganizationalIDNumber = "") Then       ' nur wenn AdressNr vorhanden
                  msgbox "Contact >" & objMailItem.SenderName & "< : No organizational ID number", vbOKOnly, WW_PROGRAMNAME
                  WW_SAVE1INMAIL = 2                  ' keine Adress-Nr vorhanden
                Else                                  ' Adress-Nr gefunden
                  Dim bsaveas As String
                  If ((ikey And WW_KEY_MAIL) <> 0) Then   ' Mail sichern
                    bsaveas = WW_MAKEFILENAME(ikey, objMailItem, objContact, 0)   ' Dateiname incl Pfad generieren
                    objMailItem.SaveAs bsaveas, olMSG   ' als Datei speichern
                    idone = 1                         ' gesichert
                  End If                              ' ende Mail sichern
                  
                  If ((ikey And WW_KEY_ATT) <> 0) Then   ' Attachments sichern
                    Dim iatt As Integer
                    If objMailItem.Attachments.Count > 0 Then
                      For iatt = 1 To objMailItem.Attachments.Count
                        Dim objAttachment As Attachment
                        Set objAttachment = objMailItem.Attachments(iatt)
                      
                        bsaveas = WW_MAKEFILENAME(ikey, objMailItem, objContact, iatt)   ' Dateiname incl Pfad generieren
                        objAttachment.SaveAsFile bsaveas                                ' Attachment sichern
                      Next                            ' naechstes Attachment
                    End If                            ' ende Attachment vorhanden
                    idone = 1                         ' gesichert
                  End If                              ' ende Attachments sichern
                  
                  If ((ikey And WW_KEY_ADR_Z) <> 0) Then      ' Zoom into Adr
                    Dim ww_name As String
                    ww_name = WW_BASEDIR
                    If (Right(ww_name, 1) <> "\") Then        ' Kontrolle auf '\'
                      ww_name = ww_name & "\"
                    End If
                    ww_name = ww_name & "wwfi.exe"            ' Programmname
  
                    Dim ww_param As String
                    ww_param = ""
                    ww_param = ww_param & " -f" & WW_CLIENT   ' Parameter: Firma
                    ww_param = ww_param & " -b" & WW_USER     ' Parameter: User
                    If (Len(WW_PASSWORD) <> 0) Then
                      ww_param = ww_param & " -p" & WW_PASSWORD  ' Parameter: Passwort
                    End If
                    ww_param = ww_param & " ""-iadr_z;" & objContact.OrganizationalIDNumber & """"

                    ShellExecute 0, "open", ww_name, ww_param, WW_BASEDIR, 0&
                  End If                              ' ende Funktion
                  
                  If ((ikey And WW_KEY_ADRA_NEW) <> 0) Then   ' neue Aktivitaet
'                   Dim ww_name As String
                    ww_name = WW_BASEDIR
                    If (Right(ww_name, 1) <> "\") Then        ' Kontrolle auf '\'
                      ww_name = ww_name & "\"
                    End If
                    ww_name = ww_name & "wwfi.exe"            ' Programmname
                    If (WW_LANGUAGE <> "D") Then
                      WW_STRPATCH ww_name, "wwfi.exe", "wwfi_e.exe"
                    End If
  
'                   Dim ww_param As String
                    ww_param = ""
                    ww_param = ww_param & " -f" & WW_CLIENT   ' Parameter: Firma
                    ww_param = ww_param & " -b" & WW_USER     ' Parameter: User
                    If (Len(WW_PASSWORD) <> 0) Then
                      ww_param = ww_param & " -p" & WW_PASSWORD  ' Parameter: Passwort
                    End If
                    ww_param = ww_param & " ""-iadra_new;" & objContact.OrganizationalIDNumber & ";" & bsaveas & """"

                    ShellExecute 0, "open", ww_name, ww_param, WW_BASEDIR, 0&
                  End If                              ' ende Funktion
                  
                  If WW_SOUND <> 0 Then               ' Mit Sound?
                    Beep                              ' Ton
                  End If                              ' ende mit Sound
                End If                                ' Ende Adress-Nr vorhanden
                '-- ~found ---
                
                If WW_SAVE2ALL = 0 Then           ' nur an 1 Email sichern?
                  Exit For                        ' ende suche nach kontakt
                End If                            ' ende nur an 1 Email sichern
              End If                              ' ende Contakt passt
            Next                                  ' naechster Contact
          End If                                  ' ende Emailadresse vorhanden
       
          If (icontactfound = 0) Then
            msgbox "Contact >" & objMailItem.SenderName & "< not found", vbOKOnly, WW_PROGRAMNAME
            WW_SAVE1INMAIL = 1                    ' nicht in den Kontakten
          End If                                  ' ende Kontakt gefunden
'       End If                                    ' ende Email-Adresse aufgeloest
'     Next                                        ' Naechster Empfaenger

      If idone <> 0 Then                          ' gesichert?
        If ((ikey And WW_KEY_MAIL) <> 0) Then     ' nur beim sichern von Mails
          If WW_DEL_AFTER_SAVE <> 0 Then          ' gleich loeschen?
            objMailItem.Delete                    ' Mail loeschen
          Else                                    ' nicht loeschen
            objMailItem.FlagStatus = olFlagComplete   ' als erledigt markieren
'           objMailItem.FlagIcon   = olYellowFlagIcon ' ab Outlook 2003 besteht auch diese Moeglichkeit
            objMailItem.Save                      ' Aenderung abspeichern
          End If                                  ' ende nicht Loeschen
        End If                                    ' ende Mail sichern
      End If                                      ' ende gesichert
'   End If                                        ' Ende Sicherung ausfuehren
' End If                                          ' ende aktiviert
End Function









Private Function ProcessEmail(myItem As Object, strBackupPath As String) As Variant
    'Saves the e-mail on the drive by using the provided path.
    'Returns TRUE if successful, and FALSE otherwise.

    Const PROCNAME As String = "ProcessEmail"

    On Error GoTo ErrorHandler

    Dim myMailItem As MailItem
    Dim strDate As String
    Dim strSender As String
    Dim strReceiver As String
    Dim strSubject As String
    Dim strFinalFileName As String
    Dim strFullPath As String
    Dim vExtConst As Variant
    Dim vTemp As String
    Dim strErrorMsg As String

    If TypeOf myItem Is MailItem Then
         Set myMailItem = myItem
    Else
        Error 1001
    End If

    'Set filename
    strDate = Format(myMailItem.ReceivedTime, EXM_OPT_FILENAME_DATEFORMAT)
    strSender = myMailItem.SenderName
    strReceiver = myMailItem.To 'All receiver, semikolon separated string
    If InStr(strReceiver, ";") > 0 Then strReceiver = Left(strReceiver, InStr(strReceiver, ";") - 1)
    strSubject = myMailItem.Subject
    strFinalFileName = EXM_OPT_FILENAME_BUILD
    strFinalFileName = Replace(strFinalFileName, "<DATE>", strDate)
    strFinalFileName = Replace(strFinalFileName, "<SENDER>", strSender)
    strFinalFileName = Replace(strFinalFileName, "<RECEIVER>", strReceiver)
    strFinalFileName = Replace(strFinalFileName, "<SUBJECT>", strSubject)
    strFinalFileName = CleanString(strFinalFileName)
    If Left(strFinalFileName, 15) = "ERROR_OCCURRED:" Then
        strErrorMsg = Mid(strFinalFileName, 16, 9999)
        Error 1003
    End If
    strFinalFileName = IIf(Len(strFinalFileName) > 251, Left(strFinalFileName, 251), strFinalFileName)
    strFullPath = strBackupPath & strFinalFileName
    
    'Save as msg or txt?
    Select Case UCase(EXM_OPT_MAILFORMAT)
        Case "MSG":
            strFullPath = strFullPath & ".msg"
            vExtConst = olMSG
        Case Else:
            strFullPath = strFullPath & ".txt"
            vExtConst = olTXT
    End Select
    'File already exists?
    If CreateObject("Scripting.FileSystemObject").FileExists(strFullPath) = True Then
        Error 1002
    End If
    
    'Save file
    myMailItem.SaveAs strFullPath, vExtConst
    
    'Return true as everything was successful
    ProcessEmail = True

ExitScript:
    Exit Function
ErrorHandler:
    Select Case Err.Number
    Case 1001:  'Not an email
        ProcessEmail = EXM_013
    Case 1002:
        ProcessEmail = EXM_014
    Case 1003:
        ProcessEmail = strErrorMsg
    Case Else:
        ProcessEmail = "Error #" & Err & ": " & Error$ & " (Procedure: " & PROCNAME & ")"
    End Select
    Resume ExitScript
End Function


Private Function CleanString(strData As String) As String

    Const PROCNAME As String = "CleanString"

    On Error GoTo ErrorHandler

    'Instantiate RegEx
    Dim objRegExp As Object
    Set objRegExp = CreateObject("VBScript.RegExp")
    objRegExp.Global = True

    'Cut out strings we don't like
    objRegExp.Pattern = EXM_OPT_CLEANSUBJECT_REGEX
    strData = objRegExp.Replace(strData, "")

    'Replace and cut out invalid strings.
    strData = Replace(strData, Chr(9), WW_FILLCHR)
    strData = Replace(strData, Chr(10), WW_FILLCHR)
    strData = Replace(strData, Chr(13), WW_FILLCHR)
    objRegExp.Pattern = "[/\\*]"
    strData = objRegExp.Replace(strData, "-")
    objRegExp.Pattern = "[""]"
    strData = objRegExp.Replace(strData, "'")
    objRegExp.Pattern = "[:?<>\|]"
    strData = objRegExp.Replace(strData, "")
    
    'Replace multiple chars by 1 char
    objRegExp.Pattern = "\s+"
    strData = objRegExp.Replace(strData, " ")
    objRegExp.Pattern = "_+"
    strData = objRegExp.Replace(strData, WW_FILLCHR)
    objRegExp.Pattern = "-+"
    strData = objRegExp.Replace(strData, "-")
    objRegExp.Pattern = "'+"
    strData = objRegExp.Replace(strData, "'")
            
    'Trim
    strData = Trim(strData)
    
    'Return result
    CleanString = strData
  
  
ExitScript:
    Exit Function
ErrorHandler:
    CleanString = "ERROR_OCCURRED:" & "Error #" & Err & ": " & Error$ & " (Procedure: " & PROCNAME & ")"
    Resume ExitScript
End Function

'Private Function GetFileDir() As String
'
'    Const PROCNAME As String = "GetFileDir"
'
'    On Error GoTo ErrorHandler
'
'    Dim ret As String
'    Dim lpIDList As Long
'    Dim sPath As String
'    Dim udtBI As BrowseInfo
'    Dim RdStrings() As String
'    Dim nNewFiles As Long
'
'    'Show a browse-for-folder form:
'    With udtBI
'        .lpszTitle = lstrcat(EXM_016, "")
'        .ulFlags = BIF_RETURNONLYFSDIRS
'    End With
'
'    lpIDList = SHBrowseForFolder(udtBI)
'    If lpIDList = 0 Then Exit Function
'
'    'Get the selected folder.
'    sPath = String$(MAX_PATH, 0)
'    SHGetPathFromIDList lpIDList, sPath
'    CoTaskMemFree lpIDList
'
'    'Strip Nulls
'    If (InStr(sPath, Chr$(0)) > 0) Then sPath = Left$(sPath, InStr(sPath, Chr(0)) - 1)
'
'    'Return Dir
'    GetFileDir = sPath
'
'ExitScript:
'    Exit Function
'ErrorHandler:
'    GetFileDir = "ERROR_OCCURRED:" & "Error #" & Err & ": " & Error$ & " (Procedure: " & PROCNAME & ")"
'    Resume ExitScript
'End Function





'***********************************************************************
'***                Code in "DieseOutlookSitzung"                    ***
'***********************************************************************
' Diesen Code einkopieren unter:
'   Microsoft Outlook Objekte  /  DieseOutlookSitzung

Sub Application_Startup()
  WW_MENUE_CREATE
End Sub
'***********************************************************************
'***             Ende Code in "DieseOutlookSitzung"                  ***
'***********************************************************************

'--------------------- Ende WW_Interface -------------------------------



