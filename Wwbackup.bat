@echo off
rem **************************************************************
rem
rem                    w w b a c k u p . b a t
rem
rem                 Datensicherung Warenwirtschaft
rem     27.11.99                                      Lachner
rem **************************************************************
rem
echo.
echo -------------------------------------------------------------
echo               CS WARENWIRTSCHAFT DATENSICHERUNG
echo -------------------------------------------------------------
echo.
echo Bitte Warenwirtschaftsprogramm beenden. 
echo.
rem ----------- Disketten loeschen -------------
echo Zunaechst muessen alle zu verwendenden Disketten geloescht werden.
echo.
:nextdel
echo.
echo Naechste zu loeschende Diskette einlegen.

rem ---- durch DOS ----
rem echo (Keine Diskette mehr loeschen: Alle Disketten entfernen)
rem pause
rem if not exist a:\NUL goto weiter
rem ---- durch ask ----
ask "Noch eine Diskette loeschen? [j/n]  ", jn
if errorlevel 2 goto weiter
rem -------------------

del a:\*.* /q > NUL
goto nextdel
:weiter
echo.  
rem ---------- Ende Disketten loeschen ---------
echo.
echo -------------------------------------------------------------
echo BEGINN DER DATENSICHERUNG
echo Bitte legen Sie die Diskette 1 ein.
echo.
pause
echo.
arj a -va a:ww *.lst 001\*.* 
echo.
echo Datensicherung beendet. Bitte Diskette entnehmen. 
echo.
pause
rem --------------------- eof wwbackup.bat -----------------------

