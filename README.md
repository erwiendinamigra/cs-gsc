# CS ERP SOFTWARE
---
[![Build Status](https://travis-ci.org/laravel/framework.svg)](https://www.cs88id.com)

CS ERP Software is an approved complete solution that covers from simple invoicing to complex inventory control system. 
The inventory control system also supports purchasing, production, shipping, sales, RMA, accounting and CRM. 
Furthermore, it is regularly updated in order to deal with current technology and also local government policy.

CS ERP is a suitable software suite to support the needs of manufacturing industry, particularly for the production process. 
It provides the mechanism for manufacturing companies to manage their production planning, resource management, and fulfill accounting rules. 
In addition, it is standardized, yet allows customization depending on specific requirements.
CS Databrige is a set of API for connecting CS ERP Software with another external system/software/hardware/devices.

## Official Documentation
---
Documentation for the software is embbeded in the software help files.

## License
---
The CS ERP Software is licensed under the CS ERP SOFTWARE Licensing.
Copyright � 1995 by CS Germany All rights reserved. 
No part of this publication may be reproduced, distributed, or transmitted in any form or by any means, including photocopying, recording, 
or other electronic or mechanical methods, without the prior written permission of CS Germany, except in the case of brief quotations embodied in critical
reviews and certain other noncommercial uses permitted by copyright law. For permission requests, write to CS Germany.

## Version History
---
### 3.64.05
* New Feature  : Import master data storage
* New Feature  : WWF->Booking administration->User defined fields
* New Feature  : Item supplier master data -> Price converter for second unit and other currency
* New Feature  : Production Overview -> Multiedit -> Set defect storage
* New Feature  : WWF->Inventory in transit 
* New Config   : 3408L -> set 1 = Transaction of item outflow/transfer is not automatically closed.
* Bug Fix      : Matrix item -> String truncation
* Bug Fix      : Rename salesman
* Bug_Fix      : Item supplier master data import, item nr is only imported 29 characters.
* Bug_Fix      : Production overview->Multiedit->set production start date, auto update production deadline.
* Bug_Fix      : Item Outflow/Transfer, expired date of serial nr/batch nr not visible.
* Improvements : Item Variants price now also usable for Purchase side transactions.
* Improvements : Transaction item -> Multi edit for Tax and Customs field.
* Improvements : Production delivery in multiple units
* Improvements : TPB Ceisa multiple updates on field definition
* Improvements : Import Item master, add field Mwst_nr
>310  97   -    -    -                             # MwSt-Nr
* Improvements : Import Discount Master data, add field discount in amount
* Improvements : E-TAX export -> Other documents support.
* New fields   : ww<transaction code>.lst
>FIELD 1 8200 22 10 1 R # einheit 2 round to up
>FIELD 1 8201 22 20 1 R # einheit 2 round to down
>FIELD 1 8202 22 30 1 R # einheit 2 round normal
>FIELD 1 8203 22 40 1 R # einheit 3 round to up
>FIELD 1 8204 22 50 1 R # einheit 3 round to down
>FIELD 1 8205 22 60 1 R # einheit 3 round normal
* New Fields   : wwbed.lst
>FIELD 0 640 5 15 4 L # item base unit
>FIELD 0 645 5 25 4 L # purchase unit
>FIELD 0 646 5 35 4 L # purchase Menge
>FIELD 0 650 5 45 4 L # packing unit
>FIELD 0 651 5 55 4 L # packing Menge
>FIELD 0 660 45 65 4 L # item user defined 0
>FIELD 0 661 45 75 4 L # item user defined 1
>FIELD 0 662 45 85 4 L # item user defined 2
>FIELD 0 663 45 95 4 L # item user defined 3
>FIELD 0 664 45 105 4 L # item user defined 4
>FIELD 0 665 45 115 4 L # item user defined 5
>FIELD 0 666 45 125 4 L # item user defined 6
>FIELD 0 667 45 135 4 L # item user defined 7
>FIELD 0 668 45 145 4 L # item user defined 8
>FIELD 0 669 45 155 4 L # item user defined 9
>FIELD 0 670 45 165 4 L # item user defined 10
>FIELD 0 671 45 175 4 L # item user defined 11
>FIELD 0 672 45 185 4 L # item user defined 12
>FIELD 0 673 45 195 4 L # item user defined 13
>FIELD 0 674 45 105 4 L # item user defined 14
>FIELD 0 675 45 115 4 L # item user defined 15
>FIELD 0 676 45 125 4 L # item user defined 16
>FIELD 0 677 45 135 4 L # item user defined 17
>FIELD 0 678 45 145 4 L # item user defined 18
>FIELD 0 679 45 155 4 L # item user defined 19

### 3.64.04
* New Feature : Export to E-tax (Indonesia) Configuration Dialog. Now it is possible to setup the tax invoice number with range.
* New Feature : Export to TPB-CEISA (Indonesia) Configuration Dialog. Configurable field mapping and unit of measurement.
* New Feature : Auto Select storage location, if only one possibility found.
* New Feature : Unit conversion dialog in Master Data item > Partlist > new
* Bug Fix 	  : Serial number matching for similar serial number
* Bug Fix     : Export to E-tax (Indonesia) for credit notes, wrong field mapping.
* Improvements : Memory optimizing in Print Out transaction


### 3.64.03
* Feature Patch : Item transfer delivery notes, item outflow delivery notes,  and consignment delivery notes transaction status default will be CLOSED, status = "F" 
                  Not allowed to delete as well.
* New Config : 3392 -> <fma_nr> = Config for TPB CEISA database client
* New Config : 3393L -> Hide "Needed material" table in Item Inflow
* Update config : 3394L -> Flag for "automatically at once" payment term in Purchase Related Form.
* Add print storage location content (from & to)
* Move menu from Statistic module : Add-on -> Production -> Cost, Add-on -> Production -> Cost Planning
* New Feature : Workflow for Activities
* New Config : 3384L -> 1 = Transaction items remark will be copied to production order remark
* New Config : 3385L -> 1 = Material request remark will be copied to transaction items remark
* New Config : 3387L -> 1 = Generate item inflow document only grouped by suppliers
* New Config : 3388L -> 1 = Allow print even the transaction is closed
* New fields : In Statistic -> List Generator
* New fields : wwsnreti.lst	
> FIELD		 1		21			# ~A<item nr>
> FIELD		 1		22			# ~C<qty>~<item nr>
* New fields : ww*.lst 		
> FIELD		 1		101			# Item Position Nr (auto incremental)
* New fields : Partlist Import
> 110  12   -    -    -                             # Purchase Price
* New fields : wwprd.lst	
> FIELD      0 		1250       # Textbig, Zeile  1
> FIELD      0 		1251       # Textbig, Zeile  2
> FIELD      0 		1252       # Textbig, Zeile  3
> FIELD      0 		1253       # Textbig, Zeile  4
> FIELD      0 		1254       # Textbig, Zeile  5
> FIELD      0 		1255       # Textbig, Zeile  6
> FIELD      0 		1256       # Textbig, Zeile  7
> FIELD      0 		1257       # Textbig, Zeile  8
> FIELD      0 		1258       # Textbig, Zeile  9
> FIELD      0 		1259       # Textbig, Zeile 10
> FIELD		 0 		131  	   # production qty in purchase unit
> FIELD		 0 		132  	   # production qty in packing unit
> FIELD		 0 		15130  	   # production Location of the parent production
> FIELD		 0 		15050  	   # Storage Location Nr of the destination storage of related Finish goods item
> FIELD		 0 		15051  	   # Storage Location Name of the destination storage of related Finish goods item
* New fields : wwvda.st
> FIELD 	0 		510        # Quantity in purchase unit
> FIELD 	0 		511        # purchase unit
> FIELD     0 		512 	   # Qty in unit 2 with 2 decimal point
> FIELD		0 		520        # Quantity in packing unit
> FIELD		0 		521        # packing unit
> FIELD 	0 		522		   # Quantity in unit 3 with 1 decimal point
> FIELD     0       523        # Quantity in unit 3 with 2 decimal point"
> FIELD		0 		603        # Serial number (without masking)
> FIELD		0 		800        # transaction's your order
* Bug fix : ww*.lst, field 1/6164
* Bug fix : Item Description in transaction items taking wrong text if the item is listed in item-supplier master data.
* Bug fix : Split production order for order related materials
* Improvements : Speed optimization in Production Parts Issue
* Improvements : Item Master Data -> Partlist -> Copy to matrix function is now extended for multi dimensional matrix
* Improvements : Master Data -> Material Request, Show last update date of supplier list




