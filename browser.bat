@echo off
rem ********************** browser.bat *******************************
echo Dieser Batch (browser.bat) dient als Beispiel fuer ein
echo Programm, das in der Lage ist, Homepages direkt
echo anzusprechen. Um ein echtes Programm zu konfigurieren,
echo sind die Einstellungen unter 
echo     System / Verwaltung / Configuration / Umgebung
echo entsprechend anzupassen. 
echo.
echo.
echo Internet-Seiten-Aufruf: %1 %2 %3 %4 %5 %6 %7 %8 %9
echo.
echo.
pause
