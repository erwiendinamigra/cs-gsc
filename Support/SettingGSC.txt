Beberapa Setting penting:
1. Invoice dicetak bersamaan dengan surat jalan. Jadi potong stok terjadi saat bikin invoice. Settingnya adalah 
	book stock at invoice di General Setting
2. Print --> flag 'Print serial numbers' nya dicentang..untuk memunculkan serial number di invoice
Pick List -> Print -> Stock Withdrawal List => untuk menampilkan barang diambil dari lokasi mana digudang
master data --> storages --> print --> contents => untuk menampilkan stok gudang termasuk batch yg ada

Proses Transaksi Order dari Cust
	1. Create AB
	2. Generate Pick List yg takeover dari AB
	3. Generate Invoice yg takeover Pick List


Perhitungan Bonus
Untuk transaksi item yang yg jadi bonus di tambahkan di user defined field beri tulisan BONUS
Jadi untuk item yang bonus, di user def transact itemnya dikasih keterangan, contoh: 'Bonus'. Nanti bikin List Generator untuk menampilkan invoice per periode. Lalu data dari ListGen itu diopen di excel. setelah itu bisa difilter transact item mana yang ada keterangan 'Bonus'
Settingnya: 
	List Generatornya pilih File InvBonus.wwl
	Klik Print
	Pilih Setting Invoice From (awal bulan) To (akhir bulan)
	SEtelah itu Klik OK 
	Klik OK di 2 dialog selanjutnya
	trus export Excel
	Gunakan Filter terhadap kolom User def 1 

Untuk Report:
Buku Stok: 
	Statistic Module --> Special Report --> Storage --> Storage Movement Detail


Setting Server
Untuk sql server, sudah ditambahkan 1 login untuk database CS:
User: csuser
Password: CScs1234