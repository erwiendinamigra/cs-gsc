Attribute VB_Name = "Module1"
Sub summary_inflow_outflow()
Attribute summary_inflow_outflow.VB_ProcData.VB_Invoke_Func = " \n14"
'
' summary_inflow_outflow Macro
'

'
    Dim MaxRow
    Range("A1:G1").Select
    With Selection.Font
        .Name = "Calibri"
        .Size = 16
        .Strikethrough = False
        .Superscript = False
        .Subscript = False
        .OutlineFont = False
        .Shadow = False
        .Underline = xlUnderlineStyleNone
        .ThemeColor = xlThemeColorLight1
        .TintAndShade = 0
        .ThemeFont = xlThemeFontMinor
    End With
    With Selection
        .HorizontalAlignment = xlCenter
        .VerticalAlignment = xlBottom
        .WrapText = False
        .Orientation = 0
        .AddIndent = False
        .IndentLevel = 0
        .ShrinkToFit = False
        .ReadingOrder = xlContext
        .MergeCells = True
    End With
    Selection.Font.Bold = True
    Range("B:G").EntireColumn.Insert
    Range("A3").Value = "Kode"
    Range("B3").Value = "Nama"
    Range("C3").Value = "Tanggal Terima"
    Range("D3").Value = "Jumlah Diterima"
    Range("E3").Value = "Tanggal Kirim"
    Range("F3").Value = "Jumlah Dikirim"
    Range("G3").Value = "Pembeli"
    Range("A3:G3").Select
    Selection.Font.Bold = True
    Selection.AutoFilter
    With Selection
        .HorizontalAlignment = xlCenter
        .VerticalAlignment = xlBottom
        .WrapText = False
        .Orientation = 0
        .AddIndent = False
        .IndentLevel = 0
        .ShrinkToFit = False
        .ReadingOrder = xlContext
        .MergeCells = False
    End With
    
    'start re-arrange data
    For i = 4 To Rows.Count
        Dim wrdarr() As String
        wrdarr() = Split(Cells(i, "A").Value, ":")
        If UBound(wrdarr) > 0 Then
            If wrdarr(0) = "Item Nr  " Then
                Cells(i, "A").Value = wrdarr(1)
            ElseIf wrdarr(0) = "Item Name" Then
                Cells(i - 1, "B").Value = wrdarr(1)
                Cells(i, "A").ClearContents
            End If
        ElseIf Cells(i, "A").Value = "***** End of list *****" Then
            MaxRow = i
            Cells(i, "A").ClearContents
        ElseIf Cells(i, "A").Value >= 2 Then
            If IsEmpty(Cells(i, "H").Value) = False Then
                If Cells(i, "L").Value = "Item inflow" Or Left(Cells(i, "L").Value, 6) = "Inflow" Then
                    Cells(i, "C").Value = Cells(i, "H").Value
                    Cells(i, "D").Value = Cells(i, "N").Value
                Else
                    isi = Split(Cells(i, "L").Value, " ")
                    If isi(0) = "Invoice" Then
                        Range(Cells(i, "G"), Cells(i, "G")).Formula = "=VLOOKUP(" & isi(1) & ",'Account receivable'!$C:$D,2,False)"
                        Cells(i, "E").Value = Cells(i, "H").Value
                        Cells(i, "F").Value = Cells(i, "T").Value
                    End If
                End If
            Else
                Cells(i, "A").ClearContents
            End If
            Cells(i, "A").ClearContents
        End If
    Next i
    
    
    'Cleanup the mess
    Columns("H:AQ").EntireColumn.Delete
    
    'Autofit
    Columns("A").AutoFit
    Columns("B").AutoFit
    Columns("C").AutoFit
    Columns("D").AutoFit
    Columns("E").AutoFit
    Columns("F").AutoFit
    Columns("G").AutoFit
    
    'clean empty rows
    'Draw Border
    
    lastCol = ActiveSheet.Range("a3").End(xlToRight).Column
    x = MaxRow
    
    Do While x > 3
        If IsEmpty(Cells(x, "A").Value) = True And IsEmpty(Cells(x, "C").Value) = True And IsEmpty(Cells(x, "E").Value) = True Then
            Rows(x).EntireRow.Delete
        End If
        x = x - 1
    Loop
    
    
    lastRow = ActiveSheet.Cells(MaxRow, lastCol).End(xlUp).Row
    lastRow2 = ActiveSheet.Cells(MaxRow, "A").End(xlUp).Row
    If lastRow > lastRow2 Then
        ActiveSheet.Range("a3", ActiveSheet.Cells(lastRow + 1, lastCol)).Select
    Else
        ActiveSheet.Range("a3", ActiveSheet.Cells(lastRow2 + 1, lastCol)).Select
    End If
    
    
    Selection.Borders(xlDiagonalDown).LineStyle = xlNone
    Selection.Borders(xlDiagonalUp).LineStyle = xlNone
    With Selection.Borders(xlEdgeLeft)
        .LineStyle = xlContinuous
        .ColorIndex = 0
        .TintAndShade = 0
        .Weight = xlThin
    End With
    With Selection.Borders(xlEdgeTop)
        .LineStyle = xlContinuous
        .ColorIndex = 0
        .TintAndShade = 0
        .Weight = xlThin
    End With
    With Selection.Borders(xlEdgeBottom)
        .LineStyle = xlContinuous
        .ColorIndex = 0
        .TintAndShade = 0
        .Weight = xlThin
    End With
    With Selection.Borders(xlEdgeRight)
        .LineStyle = xlContinuous
        .ColorIndex = 0
        .TintAndShade = 0
        .Weight = xlThin
    End With
    With Selection.Borders(xlInsideVertical)
        .LineStyle = xlContinuous
        .ColorIndex = 0
        .TintAndShade = 0
        .Weight = xlThin
    End With
    With Selection.Borders(xlInsideHorizontal)
        .LineStyle = xlContinuous
        .ColorIndex = 0
        .TintAndShade = 0
        .Weight = xlThin
    End With
    
    
End Sub
