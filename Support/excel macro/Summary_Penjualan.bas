Attribute VB_Name = "Module1"
Sub Macro1()
Attribute Macro1.VB_ProcData.VB_Invoke_Func = " \n14"
'
' Macro1 Macro
'

'
    Range("A1").Select
    ActiveCell.FormulaR1C1 = "Summary Penjualan"
    Selection.Font.Bold = True
    Selection.Font.Size = 16
    Range("A4").Select
    ActiveCell.FormulaR1C1 = "NO"
    Range("A3").Select
    Selection.ClearContents
    Range("B4").Select
    ActiveCell.FormulaR1C1 = "TANGGAL"
    Range("B3").Select
    Selection.ClearContents
    Range("C4").Select
    ActiveCell.FormulaR1C1 = "NO FAKTUR"
    Range("C3").Select
    Selection.ClearContents
    Range("D3").Select
    ActiveCell.FormulaR1C1 = "CUSTOMER"
    Range("D4").Select
    ActiveCell.FormulaR1C1 = "NAMA"
    Range("E4").Select
    ActiveCell.FormulaR1C1 = "LOKASI"
    Range("E3").Select
    Selection.ClearContents
    Range("F3").Select
    ActiveCell.FormulaR1C1 = "TANGGAL"
    Range("F4").Select
    ActiveCell.FormulaR1C1 = "J TEMPO"
    Range("G4").Select
    ActiveCell.FormulaR1C1 = "GUDANG"
    Range("G3").Select
    Selection.ClearContents
    Range("H4").Select
    ActiveCell.FormulaR1C1 = "KETERANGAN"
    Range("H3").Select
    Selection.ClearContents
    Range("I4").Select
    ActiveCell.FormulaR1C1 = "TOTAL"
    Range("I3").Select
    Selection.ClearContents
    Range("J4").Select
    ActiveCell.FormulaR1C1 = "DISKON"
    Range("J3").Select
    Selection.ClearContents
    Range("K3").Select
    Selection.ClearContents
    Range("K4").Select
    ActiveCell.FormulaR1C1 = "PPN"
    Range("L4").Select
    ActiveCell.FormulaR1C1 = "TOTAL BERSIH"
    Range("L3").Select
    Selection.ClearContents
    Range("M3").Select
    Selection.ClearContents
    Range("M4").Select
    ActiveCell.FormulaR1C1 = "PELUNASAN"
    Range("N3").Select
    Selection.ClearContents
    Range("N4").Select
    ActiveCell.FormulaR1C1 = "TGL BAYAR"
    Range("O4").Select
    ActiveCell.FormulaR1C1 = "SISA PIUTANG"
    Range("P4").Select
    ActiveCell.FormulaR1C1 = "STATUS"
    Range("A1:P1").Select
    With Selection
        .HorizontalAlignment = xlCenter
        .VerticalAlignment = xlBottom
        .WrapText = False
        .Orientation = 0
        .AddIndent = False
        .IndentLevel = 0
        .ShrinkToFit = False
        .ReadingOrder = xlContext
        .MergeCells = False
    End With
    Selection.Merge
    Columns("A:A").ColumnWidth = 4.14
    Range("A3:P4").Select
    Selection.Font.Bold = True
    With Selection
        .HorizontalAlignment = xlCenter
        .VerticalAlignment = xlBottom
        .WrapText = False
        .Orientation = 0
        .AddIndent = False
        .IndentLevel = 0
        .ShrinkToFit = False
        .ReadingOrder = xlContext
        .MergeCells = False
    End With
    Range("A4:P4").Select
    Selection.AutoFilter
    Columns("A:A").ColumnWidth = 9.71
    Columns("B:B").EntireColumn.AutoFit
    Columns("C:C").EntireColumn.AutoFit
    Columns("D:D").EntireColumn.AutoFit
    Columns("E:E").EntireColumn.AutoFit
    Columns("F:F").EntireColumn.AutoFit
    Columns("G:G").EntireColumn.AutoFit
    Columns("H:H").EntireColumn.AutoFit
    Columns("O:O").EntireColumn.AutoFit
    Columns("P:P").EntireColumn.AutoFit
    Columns("N:N").EntireColumn.AutoFit
    Columns("M:M").EntireColumn.AutoFit
    Columns("L:L").EntireColumn.AutoFit
    Columns("K:K").EntireColumn.AutoFit
    Columns("J:J").EntireColumn.AutoFit
    Columns("I:I").EntireColumn.AutoFit
    Range("D3:E3").Select
    With Selection
        .HorizontalAlignment = xlCenter
        .VerticalAlignment = xlBottom
        .WrapText = False
        .Orientation = 0
        .AddIndent = False
        .IndentLevel = 0
        .ShrinkToFit = False
        .ReadingOrder = xlContext
        .MergeCells = False
    End With
    Selection.Merge
    Range("A3").Select
    Range(Selection, Selection.End(xlToRight)).Select
    Range("A3:P4").Select
    Selection.Borders(xlDiagonalDown).LineStyle = xlNone
    Selection.Borders(xlDiagonalUp).LineStyle = xlNone
    With Selection.Borders(xlEdgeLeft)
        .LineStyle = xlContinuous
        .ColorIndex = xlAutomatic
        .TintAndShade = 0
        .Weight = xlThin
    End With
    With Selection.Borders(xlEdgeTop)
        .LineStyle = xlContinuous
        .ColorIndex = xlAutomatic
        .TintAndShade = 0
        .Weight = xlThin
    End With
    With Selection.Borders(xlEdgeBottom)
        .LineStyle = xlContinuous
        .ColorIndex = xlAutomatic
        .TintAndShade = 0
        .Weight = xlThin
    End With
    With Selection.Borders(xlEdgeRight)
        .LineStyle = xlContinuous
        .ColorIndex = xlAutomatic
        .TintAndShade = 0
        .Weight = xlThin
    End With
    With Selection.Borders(xlInsideVertical)
        .LineStyle = xlContinuous
        .ColorIndex = xlAutomatic
        .TintAndShade = 0
        .Weight = xlThin
    End With
    Selection.Borders(xlInsideHorizontal).LineStyle = xlNone
    
    'writing GrandTotal
    
    Range("A4").Select
    Selection.End(xlDown).Select
    Selection.End(xlDown).Select
    If IsEmpty(Selection.Value) = False And ActiveCell.MergeCells = False Then
        Selection.ClearContents
    End If
    
    Range("A4").Select
    Cells(Rows.Count, 1).End(xlUp).Offset(2, 0).Select
    Range(Selection, Selection.Offset(0, 7)).Select
    
    With Selection
        .HorizontalAlignment = xlCenter
        .VerticalAlignment = xlBottom
        .WrapText = False
        .Orientation = 0
        .AddIndent = False
        .IndentLevel = 0
        .ShrinkToFit = False
        .ReadingOrder = xlContext
        .MergeCells = True
    End With
    ActiveCell.FormulaR1C1 = "GRANDTOTAL"
    Selection.Font.Bold = True
    
    'data border
    lastCol = Range("a3").End(xlDown).End(xlToRight).Column
    lastRow = Cells(65536, "A").End(xlUp).Row
    ActiveSheet.Range("a3", Cells(lastRow, lastCol)).Select
    
    Selection.Borders(xlDiagonalDown).LineStyle = xlNone
    Selection.Borders(xlDiagonalUp).LineStyle = xlNone
    With Selection.Borders(xlEdgeLeft)
        .LineStyle = xlContinuous
        .ColorIndex = 0
        .TintAndShade = 0
        .Weight = xlThin
    End With
    With Selection.Borders(xlEdgeTop)
        .LineStyle = xlContinuous
        .ColorIndex = 0
        .TintAndShade = 0
        .Weight = xlThin
    End With
    With Selection.Borders(xlEdgeBottom)
        .LineStyle = xlContinuous
        .ColorIndex = 0
        .TintAndShade = 0
        .Weight = xlThin
    End With
    With Selection.Borders(xlEdgeRight)
        .LineStyle = xlContinuous
        .ColorIndex = 0
        .TintAndShade = 0
        .Weight = xlThin
    End With
    With Selection.Borders(xlInsideVertical)
        .LineStyle = xlContinuous
        .ColorIndex = 0
        .TintAndShade = 0
        .Weight = xlThin
    End With
    With Selection.Borders(xlInsideHorizontal)
        .LineStyle = xlContinuous
        .ColorIndex = 0
        .TintAndShade = 0
        .Weight = xlThin
    End With
    
    'sisa piutang & status
    x = 5
    Range(Cells(x, "A"), Cells(x, "A")).Select
    Do While IsEmpty(Selection.Value) = False
        If IsEmpty(Range(Cells(x, "M"), Cells(x, "M")).Value) = True Or Range(Cells(x, "M"), Cells(x, "M")).Value = 0 Then
            Range(Cells(x, "O"), Cells(x, "O")).Value = 0
        Else: Range(Cells(x, "O"), Cells(x, "O")).Value = Range(Cells(x, "L"), Cells(x, "L")).Value - Range(Cells(x, "M"), Cells(x, "M")).Value
        End If
        'status
        If Range(Cells(x, "O"), Cells(x, "O")).Value = 0 And IsEmpty(Range(Cells(x, "N"), Cells(x, "N")).Value) = False Then
            Range(Cells(x, "P"), Cells(x, "P")).Value = "LUNAS"
        Else: Range(Cells(x, "P"), Cells(x, "P")).Value = "NEW"
        End If
        
        x = x + 1
        Range(Cells(x, "A"), Cells(x, "A")).Select
    Loop
        
    'total pelunasan
    Cells(Rows.Count, "l").End(xlUp).Offset(0, 1).Select
    startrow = Cells(ActiveCell.Row, "m").End(xlUp).Row
    endrow = 5
    Range(Cells(startrow, "m"), Cells(endrow, "m")).Select
    jml = WorksheetFunction.Sum(Selection)
    Cells(Rows.Count, "l").End(xlUp).Offset(0, 1).Select
    ActiveCell.FormulaR1C1 = jml
    
    'sisa piutang
    Cells(Rows.Count, "m").End(xlUp).Offset(0, 2).Select
    startrow = Cells(ActiveCell.Row, "o").End(xlUp).Row
    endrow = 5
    Range(Cells(startrow, "o"), Cells(endrow, "o")).Select
    jml = WorksheetFunction.Sum(Selection)
    Cells(Rows.Count, "m").End(xlUp).Offset(0, 2).Select
    ActiveCell.FormulaR1C1 = jml
    
End Sub
