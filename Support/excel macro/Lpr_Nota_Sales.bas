Attribute VB_Name = "Module11"
Sub Lpr_Nota_Sales()
Attribute Lpr_Nota_Sales.VB_ProcData.VB_Invoke_Func = " \n14"
'
' Lpr_Nota_Sales Macro
'

'
    Range("A1:J1").Select
    Selection.Font.Bold = True
    With Selection.Font
        .Name = "Calibri"
        .Size = 16
        .Strikethrough = False
        .Superscript = False
        .Subscript = False
        .OutlineFont = False
        .Shadow = False
        .Underline = xlUnderlineStyleNone
        .ThemeColor = xlThemeColorLight1
        .TintAndShade = 0
        .ThemeFont = xlThemeFontMinor
    End With
    Selection.Merge
    With Selection
        .HorizontalAlignment = xlCenter
        .VerticalAlignment = xlBottom
        .WrapText = False
        .Orientation = 0
        .AddIndent = False
        .IndentLevel = 0
        .ShrinkToFit = False
        .ReadingOrder = xlContext
        .MergeCells = True
    End With
    
    Cells(Rows.Count, 1).End(xlUp).Select
    If IsEmpty(Selection.Value) = False Then
        Selection.ClearContents
    End If
    
    Range("B4").Select
    ActiveCell.FormulaR1C1 = "NO"
    Range("A4").Select
    ActiveCell.FormulaR1C1 = "TANGGAL"
    Range("A3:B3").Select
    With Selection
        .HorizontalAlignment = xlCenter
        .VerticalAlignment = xlBottom
        .WrapText = False
        .Orientation = 0
        .AddIndent = False
        .IndentLevel = 0
        .ShrinkToFit = False
        .ReadingOrder = xlContext
        .MergeCells = True
    End With
    ActiveCell.FormulaR1C1 = "Order Confirmation"
    Range("C3").Select
    ActiveCell.FormulaR1C1 = "FAKTUR"
    Range("D3").Select
    Selection.ClearContents
    Range("C4").Select
    ActiveCell.FormulaR1C1 = "TANGGAL"
    Range("D4").Select
    ActiveCell.FormulaR1C1 = "NO"
    Range("C3:D3").Select
    With Selection
        .HorizontalAlignment = xlCenter
        .VerticalAlignment = xlBottom
        .WrapText = False
        .Orientation = 0
        .AddIndent = False
        .IndentLevel = 0
        .ShrinkToFit = False
        .ReadingOrder = xlContext
        .MergeCells = False
    End With
    Selection.Merge
    Range("E3").Select
    ActiveCell.FormulaR1C1 = "CUSTOMER"
    Range("E4").Select
    Columns("E:E").EntireColumn.AutoFit
    Range("F3").Select
    ActiveCell.FormulaR1C1 = "NAMA BARANG"
    Range("F4").Select
    Columns("F:F").EntireColumn.AutoFit
    Range("G3").Select
    ActiveCell.FormulaR1C1 = "JUMLAH"
    Range("A3:B3").Select
    ActiveCell.FormulaR1C1 = "ORDER CONFIRMATION"
    Range("H3").Select
    ActiveCell.FormulaR1C1 = "HARGA"
    Range("I3").Select
    ActiveCell.FormulaR1C1 = "DISKON (%)"
    Range("J3").Select
    ActiveCell.FormulaR1C1 = "SUBTOTAL"
    Range("J4").Select
    Columns("I:I").EntireColumn.AutoFit
    Range("A3:K4").Select
    Range("J4").Activate
    Selection.Font.Bold = True
    Range("A3:B4").Select
    Range(Selection, Selection.End(xlToRight)).Select
    Range("A3:J4").Select
    Selection.Borders(xlDiagonalDown).LineStyle = xlNone
    Selection.Borders(xlDiagonalUp).LineStyle = xlNone
    With Selection.Borders(xlEdgeLeft)
        .LineStyle = xlContinuous
        .ColorIndex = xlAutomatic
        .TintAndShade = 0
        .Weight = xlThin
    End With
    With Selection.Borders(xlEdgeTop)
        .LineStyle = xlContinuous
        .ColorIndex = xlAutomatic
        .TintAndShade = 0
        .Weight = xlThin
    End With
    With Selection.Borders(xlEdgeBottom)
        .LineStyle = xlContinuous
        .ColorIndex = xlAutomatic
        .TintAndShade = 0
        .Weight = xlThin
    End With
    With Selection.Borders(xlEdgeRight)
        .LineStyle = xlContinuous
        .ColorIndex = xlAutomatic
        .TintAndShade = 0
        .Weight = xlThin
    End With
    With Selection.Borders(xlInsideVertical)
        .LineStyle = xlContinuous
        .ColorIndex = xlAutomatic
        .TintAndShade = 0
        .Weight = xlThin
    End With
    Selection.Borders(xlInsideHorizontal).LineStyle = xlNone
    Range("A3:J4").Select
    With Selection
        .HorizontalAlignment = xlGeneral
        .VerticalAlignment = xlBottom
        .WrapText = False
        .Orientation = 0
        .AddIndent = False
        .IndentLevel = 0
        .ShrinkToFit = False
        .ReadingOrder = xlContext
    End With
    With Selection
        .HorizontalAlignment = xlCenter
        .VerticalAlignment = xlBottom
        .WrapText = False
        .Orientation = 0
        .AddIndent = False
        .IndentLevel = 0
        .ShrinkToFit = False
        .ReadingOrder = xlContext
    End With
    Range("E3").Select
    Columns("A:A").ColumnWidth = 14.71
    Columns("A:A").ColumnWidth = 12.71
    Columns("A:A").EntireColumn.AutoFit
    
lastCol = "J"
lastRow = ActiveSheet.Cells(65536, "J").End(xlUp).Row
ActiveSheet.Range("a3", ActiveSheet.Cells(lastRow, lastCol)).Select
    
    
    Selection.Borders(xlDiagonalDown).LineStyle = xlNone
    Selection.Borders(xlDiagonalUp).LineStyle = xlNone
    With Selection.Borders(xlEdgeLeft)
        .LineStyle = xlContinuous
        .ColorIndex = xlAutomatic
        .TintAndShade = 0
        .Weight = xlThin
    End With
    With Selection.Borders(xlEdgeTop)
        .LineStyle = xlContinuous
        .ColorIndex = xlAutomatic
        .TintAndShade = 0
        .Weight = xlThin
    End With
    With Selection.Borders(xlEdgeBottom)
        .LineStyle = xlContinuous
        .ColorIndex = xlAutomatic
        .TintAndShade = 0
        .Weight = xlThin
    End With
    With Selection.Borders(xlEdgeRight)
        .LineStyle = xlContinuous
        .ColorIndex = xlAutomatic
        .TintAndShade = 0
        .Weight = xlThin
    End With
    With Selection.Borders(xlInsideVertical)
        .LineStyle = xlContinuous
        .ColorIndex = xlAutomatic
        .TintAndShade = 0
        .Weight = xlThin
    End With
    With Selection.Borders(xlInsideHorizontal)
        .LineStyle = xlContinuous
        .ColorIndex = xlAutomatic
        .TintAndShade = 0
        .Weight = xlThin
    End With

    Range("A4:J4").Select
    Selection.AutoFilter
    Columns("G:G").EntireColumn.AutoFit
    Columns("H:H").EntireColumn.AutoFit
    Columns("J:J").EntireColumn.AutoFit
    Columns("I:I").EntireColumn.AutoFit
    Columns("F:F").EntireColumn.AutoFit
    Columns("E:E").EntireColumn.AutoFit
    Columns("A:A").ColumnWidth = 16.29
    Columns("C:C").EntireColumn.AutoFit
    Columns("B:B").EntireColumn.AutoFit
    Columns("D:D").EntireColumn.AutoFit
    ActiveWindow.LargeScroll ToRight:=0
    ActiveWindow.ScrollColumn = 2
    ActiveWindow.ScrollColumn = 3
    Range("I12").Select
    Selection.Font.Bold = True
    Range("E3").Select
    ActiveCell.FormulaR1C1 = ""
    Range("E4").Select
    ActiveCell.FormulaR1C1 = "CUSTOMER"
    Range("F3").Select
    ActiveCell.FormulaR1C1 = ""
    Range("F4").Select
    ActiveCell.FormulaR1C1 = "NAMA BARANG"
    Range("G3").Select
    ActiveCell.FormulaR1C1 = ""
    Range("G4").Select
    ActiveCell.FormulaR1C1 = "JUMLAH"
    Columns("G:G").EntireColumn.AutoFit
    Range("H3").Select
    ActiveCell.FormulaR1C1 = ""
    Range("H4").Select
    ActiveCell.FormulaR1C1 = "HARGA"
    Columns("H:H").EntireColumn.AutoFit
    Range("I3").Select
    ActiveCell.FormulaR1C1 = ""
    Range("I4").Select
    ActiveCell.FormulaR1C1 = "DISKON(%)"
    Columns("I:I").EntireColumn.AutoFit
    Range("J3").Select
    ActiveCell.FormulaR1C1 = ""
    Range("J4").Select
    ActiveCell.FormulaR1C1 = "SUBTOTAL"
    Columns("J:J").EntireColumn.AutoFit
    
    Range("G4").Select
    Selection.End(xlDown).Select
    Selection.End(xlDown).Select
    If Selection.Value > 0 Then
        Selection.ClearContents
    End If
    
    
    Range("J5").Select
    Range(Selection, Selection.End(xlDown)).Select
    Dim total
    total = WorksheetFunction.Sum(Selection)
    Cells(Rows.Count, 10).End(xlUp).Offset(1, 0).Select
    ActiveCell.FormulaR1C1 = total
    ActiveCell.Offset(0, -1).Select
    ActiveCell.Value = "TOTAL"
    Selection.Font.Bold = True
    
End Sub
