@echo off
rem ********************* waehlen.bat ***************************
echo Dieser Batch (waehlen.bat) dient als Beispiel fuer ein
echo Programm, das in der Lage ist, Telefon-Nummern direkt
echo zu waehlen. Um ein echtes Programm zu konfigurieren,
echo sind die Einstellungen unter 
echo     System / Verwaltung / Configuration / Umgebung
echo entsprechend anzupassen. 
echo.
echo.
echo Tel-Nr waehlen: %1 %2 %3 %4 %5 %6 %7 %8 %9
echo.
echo.
pause
