
Um Zeit zu sparen, schreibe ich das nicht zum jeweiligen LOP-Punkt

Aenderungen in ww
-----------------
1) Leistungseditor
   Lange Artikeltexte: Es wird jetzt auch die erste Artikelzeile bei Bedarf umgebrochen.
   
2) Stellenanzahl der Textspalte bei Formularen
   Dieser Parameter ist jetzt in der Oberflaeche: "Configuration / Formulare" (Ganz unten)
   
3) Indra: wwxx.lst is changed to wwbr.lst after reprint
   I reallly don't know what you tried to reprint the item inflow protocol.
   In fact there was no possibility to open and to reprint the "Universal form" of a transaction until now.
   In the latest version the reprint is available in the menu:
               ww / Processing / Universal form



Aenderungen in wwf.exe
----------------------
1) Depreciation per month
   There is a new checkbox in the "wwf / System / Configuration / Asset accounting": Monthly depreciation
   If activated, depreciation will be 1 month more every time the depreciation is running.

   Please note: It is really impossible to implement and test a new functionality within a few hours.
   Therefore it may be buggy. If something doesn't work, let me know.
   
2) Balance sheet classification
   In the latest version a record beginning with "3" is also classified as "Passiva" (I didn't test it)


3) COGS of manufactured in house items
   I assume, that this issue is solved because you found out that there was no average purchase price.
   If it is not solved, please let me know.
   
   

   