        
wwfhi.txt

                      Version History  "CS FIBU"
                      --------------------------
                         
08.12.04 V0.04   - Fehlerbehebungen
22.01.05 V0.07   - Berechtigung 'FIBU' zum Programmstart noetig
10.02.05 V0.08   - ELSTER: Umsatzsteuer-Voranmeldung
17.02.05 V0.10   - ELSTER: Ab jetzt keine Testdaten mehr
20.03.05 V0.11   - !!!!! Offene Posten: Neues Feld: Parent !!!!!
23.03.05 V0.11.01- Automatik bei OP-Ausgleich schaltbar
28.03.05 V0.12.01- Neue Tabelle: Automatik-Konten
03.05.05 V0.12.02- Bug fix: Wenn ein Steuerkonto mehrmals in verschiedene
                   Autokonto-Datensaetze eingetragen ist, dann zusaetzlich
                   das Netto-Konto pruefen, damit die Werte zum
                   richtigen Autokonto (und damit zum richtigen MwSt-Satz)
                   addiert werden.
07.05.05 V0.12.03- Erweiterte OP Verwaltung
08.05.05         - Bug fix: Keine MwSt bei Buchen von Vorgaengen mit
                   Aufteilungsbuchungen.
25.10.05 V0.12.04- Bug fix: Fehler bei mehr als 255 Buchungen im Stapel
                   (Tabelle bleibt offen)
20.11.05 V0.13.01- !!!!! Stapel: Neues Feld: WaehrungsNr !!!!!
11.01.06 V0.13.02- !!!!! Anlagen: Neues Feld: Menge !!!!!
01.02.06 V0.13.03- Indonesische Banken-Schnittstelle
12.02.06 V0.13.04- ELSTER-Schnittstelle Version 03
05.03.06 V0.14.01- Aktivierung des Feldes "Abschreibung bis" in den Anlagen
***** DEMO IN DER HOMEPAGE  *****
15.03.06 V0.14.02- Fehlerbehebung in den Steuerbetraegen bei Ust-voranmeldung
                   und Ust-Journal                                      
18.03.06 V0.14.03- Bilanz mit Summe Deb/Cred
25.03.06 V0.14.04- Fehler in Bilanz korrigiert
30.03.06 V0.14.05- Bilanz prueft mehr Plausibilitaeten. Erweiterte Buchungs-Diagnose
12.07.06 V0.14.06- Verhinderung von doppelten Kennzahlen bei der Uebergabe an Elster
17.12.06 V0.14.07- Import Konten: Zusaetzl. Abfrage auf Update bestehender Saetze
                   Neu/Update von Konten wegen 19% MwSt
                   SKR03       SKR04
                   1517        1184
                   1518        1186
                   1574        1413
                   1576        1406
                   1577        1407
                   1718        3272
                   1774        3804
                   1776        3806
                   1787        3837
                   3120        5920
                   3140        5940
                   3400        5400
                   3720        5720
                   3735        5735
                   3736        5736   
                   8400        4400
                   8410        4410
                   8720        4720
                   8735        4735
                   8736        4736
                   -           4730
29.12.06 V0.14.08- Neue Funktion: Import offene Posten
30.01.07 V0.15.02- Fehlerbehebung : Fehlender Skonto bei OPs ohne Buchung
                   (Nach Jahreswechsel bzw importierte OPs)
                 - Prio bei der Erloeskonten-Ermittlung geaendert:
                   Standard-Auslandskonten jetzt in der Prio niedriger als
                   Konten in Artikel und Artikelgruppe.                    
07.02.07 V0.15.03- !!!!! Off.Post    : Neue Felder       : Zahlcode-Nr, MwSt-Nr
14.02.07 V0.15.04- !!!!! Adressen    : Feldverlaengerung : Anwenderdef. Felder auf 60
                 - !!!!! MwSt        : Neue Felder       : Konten fuer Auslands-Skonti
21.02.07 V0.15.05- Bug fix: Vorzeichenfehler bei Gutschriften mit Aufteilungsbuchung
22.02.07 V0.15.06- Bug fix: Fehler bei Ermittlung des MwSt-Satzes bei Skonti
10.03.07 V0.15.07- WAehrungs-Historie wird bei Fremdwaehrung verwendet
14.11.07         - Bug fix: Auch bei Aufteilungsbuchungen den richtigen MwSt-Satz aus dem
                   Autokonto in den OP schreiben. 
                 - Anzeige korrigieren: OPs nur anzeigen, wenn nicht storniert/kein Storno
26.12.07 V0.16.01- !!!!! Stapel       : Feldverlaengerung : Buchungstext auf 60                                
                 - !!!!! Buchungen    : Feldverlaengerung : Buchungstext auf 60             
17.01.08 V0.17.01- !!!!! Offene Posten: Neues Feld        : Vorgangs-Nr
26.02.08 V0.17.02- Bug fix: Beim Jahreswechsel werden Buchungsnummern aus OPs des aktuellen Jahres geloescht
27.03.08 V0.17.03- Globale Werte Vorgang (Fracht, Verpackung etc) werden einbezogen
21.04.08 V0.17.04- Bug fix: Skontobuchung auf falscher Seite bei Lastschrift aus dem Modul 'Zahlungsverkehr'
22.04.08 V0.17.05- Bug fix: Keine vollstaendigen Betraege nach Button "FW" in der Buchungsmaske
09.05.08         - Bug fix: Summe Debitoren/Creditoren in der Bilanz waren ohne EBs
16.06.08 V0.17.06- Bei Ermittlung der Steuerwerte Abfrage auf ein Adressenkonto in der
                   Buchung entfernt, wegen Fehlermeldung aus dem Feld.
30.06.08 V0.17.07- Vorgaenge ohne Adressenkonto werden nicht mehr gebucht
                   (kein Haengenbleiben mehr in einer Aufteilungsbuchung)
04.07.08 V0.17.08- Der Waehrungskurs aus einem Vorgang hat IMMER Prioritaet vor dem Tageskurs zum Datum der Buchung
11.07.08 V0.17.09- Jetzt auch im UVA:Jounal: Bei Ermittlung der Steuerwerte Abfrage auf ein Adressenkonto in der
                   Buchung entfernt, wegen Fehlermeldung aus dem Feld. (siehe auch 16.06.08)
                 - Beim Datum eines OP hat nicht mehr das Datum der zugrundeliegenden Buchung
                   die Prioritaet. (Bei Teilzahlungen ist sonst urspruengliches Datgum des OP nicht mehr gueltig)
                 - Neue Funktion: "Tools / OP Verwaltung"                   
20.07.08 V0.17.10- Ermittlung der Steuerbetraege: USt/Vst nur aufnehmen, wenn nicht durch Auto-Konnto abgedeckt.
                   Autokonten immer aufnehmen. D.h. Prioritaet gedreht.
                   Es darf jetzt auch ein Standard MwSt-Konto im Automatikkonto eingetragen sein.
07.10.08         - Neues Kommando: Zahlungsavis                   
20.10.08 V0.17.11- Neu: Buchen von Bestellungen aus der Warenwirtschaft
03.12.08 V0.17.12- !!!!! Stapel       : Neues Feld       : WE-Stapel-Nr
                 - !!!!! Buchungen    : Neues Feld       : WE-Stapel-Nr
                 - !!!!! Offene Posten: Neues Feld       : WE-Stapel-Nr
                 - Neue Funktion: Buchen von WE-Stapeln
11.12.08 V0.17.13- Versand Elster-Daten nicht mehr ueber ESTP (alt), sondern ueber ESTPoverHTTPS
                 - Konfiguration Steuerdaten: Neue Felder fuer Proxy-Einstellungen
01.01.09 V0.18.01- !!!!! Stapel       : Neue Felder      : Produktion-Nr, Stueckzahl
                 - !!!!! Buchungen    : Neues Feld       : Produktion-Nr
                 - !!!!! Offene Posten: Neues Feld       : Produktion-Nr
06.01.09 V0.18.02- Bei Buchen aus der Warenwirtschaft: Umstieg von numerischen auf alfanumerische Konten
31.01.09 V0.19.01- Neue Konten: Gewinn/Verlust durch Kursdifferenzen
                   !!!!! Offene Posten: Neue Felder      : Fuer Kursdifferenzen
11.02.09 V0.19.02- Neues Kommando:  Abschluss Fremdwaehrungen
01.03.09 V0.19.03- Abschluss Fremdwaehrungen: Nur noch 1 Buchung pro Konto
                 - Bug fix: Rundungsfehler bei Import Vorgang (Diff. zw. Warenwirtschaft und FiBu)
13.03.09 V0.19.04- !!!!! Stapel       : Neue Felder      : anwenderdef.
                   !!!!! Buchungen    : Neue Felder      : anwenderdef.
01.04.09 V0.20.01- !!!!! Anlagen      : Neue Felder      : Fremdwahrung
06.04.09         - Bug fix: "Tools / OP-Verwaltung": Programm wird beendet, wenn eine Zeile mit OP ohne Framdwaehrung angeklickt wird.
                 - Verbesserung OP-Ausgleich im zusammenhang mit Fremdwaehrungen:
                   Das Verhalten in der Buchungsmaske ist jetzt insofern anders, dass nur noch die OPs mit
                   derselben Waehrung wie der Stapelsatz zum Ausgleich angeboten werden.
                   D.h. die OPs werden erst angezeigt, wenn in der Eingabezeile die Waehrung eingetragen ist.
08.04.09         - Laden von Eingabefeldern. Es wird jetzt VOR dem Laden des Feldes in der Maske auf die entsprechende Anzahl Stellen
                   gerundet. In seltenen Faellen (z.B. bei 625,625) rundet der Compiler bei Laden in das Feld nicht richtig 
                   (Hier ergibt sich dann 625,62)
09.04.09 V0.20.02- Eigener OP fuer Steuerbetrag configurierbar
13.04.09         - OP-Ausgleich: Verwendung des Waehrungskurses aus dem OP statt des Tageskurses konfigurierbar.
21.07.09         - Bug fix: Anzeige Betrag in Tools / OP-Verwaltung
                 - OPs: Auch kleinere Betraege als 5% des Gesamtbetrags sind als Rest waehlbar.
07.09.09 V0.20.03- !!!!! Autokonten   : Feldverlaengerung : Kontonummern auf 30       
                 - !!!!! Anlagen      : Feldverlaengerung : Kontonummern auf 30
                 - !!!!! Banken       : Feldverlaengerung : Kontonummern auf 30
                 - !!!!! Buchungen    : Feldverlaengerung : Kontonummern auf 30
                 - !!!!! Mw-Steuern   : Feldverlaengerung : Kontonummern auf 30
                 - !!!!! Off. Posten  : Feldverlaengerung : Kontonummern auf 30
                 - !!!!! Stapel       : Feldverlaengerung : Kontonummern auf 30
07.09.09 V0.20.04- !!!!! Kostenstellen: Neue Tabelle
                 - !!!!! Kst-Buchungen: Neue Tabelle
                 - !!!!! Kst-Aufteilg.: Neue Tabelle
07.09.09 V0.20.05- !!!!! Kostenarten  : Neue Tabelle
15.10.09         - Kontenermittlung auch im Baum der Artikelgruppen
                 - Erzeugung Automatikkonten: SKR3: Korrektur Konto 8950 -> 8120
                                              SKR4: Korrektur Konto 4690 -> 4120
17.10.09         - Automatischer Aufruf Grundmodul, wenn Update der Datenbank noetig
07.11.09         - Anzeige Version History automatisch oder manuell
19.12.09         - Beschleunigung der Buchungsmaske, wenn sehr viele Buchungen vorhanden ("Boxen" unten fuellen)
21.12.09         - Neue Funktion: Gewinn/Verlust aufgeschluesselt
31.12.09         - Buchungstext: Wieder mal zurueckgeaendert. Aktuelle Funktion: Wenn Vorgangstext vorhanden, wird er uebernommen, ansonsten der Name1 aus der Adresse
                 - Buchungsmaske: Vorgaenge werden nur dann angezeigt, wenn die Berechtigung zur ANZEIGE des zugehoerigen Formulartyps besteht.
04.01.10 V0.20.06- ELSTER-dll erst referenzieren, wenn benoetigt. D.h. wwf.exe laeuft auch, wenn ELSTER nicht vorhanden ist.
09.01.10         - Bug fix: Druck Stapel: Endlose Schleife, wenn mehr als 1 Stapel vorhanden und "andere" Stapelinhalte ebenfalls selektiert sind.
17.01.10         - Buchungsmaske: Neue Funktion: Filter fuer Daten aus der Warenwirtschaft
27.02.10         - Bug fix: Buchungsmaske: Edit OP-Ausgleich in Fremdwaehrung. Bisher war kein Rest < 5% moeglich, da automat. Skontoberechnung 
07.05.10 V0.20.07- Programm arbeitet ab jetzt nur noch mit Registrierung 
10.05.10         - Diverse Kommandos nur noch mit "Professional" verfuegbar.
26.05.10         - Fremdwaehrung wird ab jetzt auch in Steuerbuchungen geschrieben, da z.B. fuer Bilanz in Fremdwaehrung gebraucht (Sonst Kursfehler)
07.07.10         - Bei gespeicherten Stapeln sind keinerlei Referenzen auf OPs und Kostenstellen-Buchungen mehr vorhanden.
04.08.10         - !!!!! BKZ          : Feldverlaengerung : BKZ-Nr auf 30
09.08.10         - Buchungs-Editor: Bei Verlassen werden alle inkonstistenten Buchungen geloescht.
24.11.10         - Bug fix: Storno-Funktion
25.11.10 V0.20.08- !!!!! Ktr-Buchungen: Neue Tabelle
                 - !!!!! Ktr-Aufteilg.: Neue Tabelle
09.12.10         - Buchungsmaske: Anzeige "Warenwirtschaft": Warenzu-/abgaenge werden nur noch dann angezeigt, wenn nicht bereits parallel
                   der zugehoerige Vorgang angezeigt wird.
                   (Beispiel: Eine Warenzubuchung wird dann nicht angezeigt, wenn der Warenzugang aus einer Bestellung resultiert und die
                   Anzeige der Bestellungen eingeschaltet ist)
                 - Buchungsmaske: Buchen von eingangsrechnungen: Vorgangskopfdaten / "Ihre Bestell-Nr" wird in "Beleg-Nr" uebernommen.
                   (Dort sollte die Original-Rechnungs-Nr des Lieferanten stehen)
03.01.11 V0.20.09- COGS-Buchung laut IFRS, wenn eingeschaltet
06.01.11 V0.20.10- Jahresabschluss: Kostenrechnung integriert
22.01.11         - Kostenrechnung: Es wird nicht mehr der Bruttobetrag, sondern der Nettobetrag vom Stapel uebernommen.
08.02.11 V0.20.11- Bug fix: Abschluss: Wenn vor einem WIEDERHOLUNGSabschluss in beiden Jahren parallel neue OPs angelegt werden, 
                   dann werden die OPS beim Wiederholungsabschluss gegenseitig ueberschrieben.                                      
18.03.11 V0.20.12- !!!!! Stapel       : Neue Felder      : Zweiter Waehrungskurs fuer MwSt
26.04.11         - Abschluss Waehrungen: Nur noch Sachkonten in den abschluss einbeziehen. (Bei Adresskonten ist sonst der Saldo falsch)
10.05.11         - Verhinderung, dass ein Vorgang mehrmals verbucht wird (z.B. wenn mehr als eine buchungsmaske parallel offen ist)
                 - Buchungsmaske: Beschleunigung der Ausgabe der OPs. (Keine unnoetige Ausgabe) 
                                  Weitere Beschleunigung durch setzen Config 2010110 auf "2".

                   
                                                                         
==========================================================================
Ausblick auf naechste Version(en):

Konten (bei Bedarf)
             B (Bilanz) +  A (Aktiva)
             B (Bilanz) +  P (Passiva)
             B (Bilanz) +  W (Wechselkonto)
             G (GuV)    +  E (Ertrag)
             G (GuV)    +  A (Aufwand)

Stapel:  Stapelname, statt wie bisher im Benutzer. Evtl Flag: "Gespeicherter Stapel"

ww_op    Neue Felder: Zahlcode-Nr, MwSt-Nr


-------------------------- eof 'wwfhi.txt' -------------------------------
